class CreateHomework < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.belongs_to :admin_user
      t.text :content

      t.timestamps
    end
  end
end
