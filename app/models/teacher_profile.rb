class TeacherProfile < ActiveRecord::Base

  belongs_to :user

  monetize :price_units
end
