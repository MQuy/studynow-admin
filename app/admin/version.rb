ActiveAdmin.register PaperTrail::Version do
  menu false

  scope('Users', default: true) { |scope| scope.where(item_type: 'User') }
  scope('Payments') { |scope| scope.where(item_type: 'Payment') }
  scope('Courses') { |scope| scope.where(item_type: 'Course') }
  scope('Lesson Schedules') { |scope| scope.where(item_type: 'LessonSchedule') }

  index do
    id_column
    if params[:scope] == 'payments'
      render 'admin/versions/payments', context: self
    elsif params[:scope] == 'courses'
      render 'admin/versions/courses', context: self
    elsif params[:scope] == 'lesson_schedules'
      render 'admin/versions/lesson_schedules', context: self
    else
      render 'admin/versions/users', context: self
    end
    column('Whodunnit') do |version|
      admin = AdminUser.find_by(id: version.whodunnit)
      if admin
        link_to admin.full_name, admin_user_path(admin.id)
      else
        version.whodunnit
      end
    end
    column :event
    column :created_at
    actions
  end

  controller do
    def revert
      @version = PaperTrail::Version.find(params[:id])
      @target = @version.reify
      @target.save

      flash[:notice] = "You reverted successfully to version ##{@version.id} at #{@version.created_at.strftime('%d-%m-%Y %H:%M')}."
      redirect_to :back
    end
  end
end
