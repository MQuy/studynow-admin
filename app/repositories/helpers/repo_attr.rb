module Helpers
  module RepoAttr
    def attr_record(record_name)
      klass = record_name.to_s.classify.constantize
      attrs = klass.send(:attribute_names) + [:persisted?, :valid?]

      instance_eval do
        attr_accessor record_name

        def create(*args)
          repo = new(*args)
          repo.tap { |r| r.save }
        end
      end

      define_singleton_method :load do |target|
        repo = new
        repo.tap { |r| r.send("#{record_name}=", target) }
      end

      define_singleton_method :find do |id|
        target = klass.find(id)
        load(target)
      end

      define_singleton_method :find_by do |args|
        target = klass.find_by(args)
        target ? load(target) : nil
      end

      define_method(:save) { send(record_name).send(:save) }
      define_method(:save!) { send(record_name).send(:save!) }
      define_method(:destroy) { send(record_name).send(:destroy) }
      define_method(:errors) { send(record_name).send(:errors).send(:to_a) }
      define_method(:object) { send(record_name) }

      attrs.each do |attr|
        define_method(attr) { send(record_name).send(attr) }
      end
    end
  end
end
