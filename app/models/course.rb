class Course < ActiveRecord::Base
  include UniqueCode

  enum status: [:in_process, :completed, :cancel]

  monetize :price_units
  has_paper_trail on: [:create, :update, :destroy]

  belongs_to :student, class_name: Student
  belongs_to :creator, class_name: AdminUser, foreign_key: :creator_id
  has_many :course_schedules, dependent: :destroy
  has_many :lesson_schedules, dependent: :destroy
  has_many :course_notes, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :course_comments, class_name: Comment, as: :commentable, dependent: :destroy

  has_many :course_teachers, dependent: :destroy
  has_many :teachers, through: :course_teachers

  has_one :active_course_teacher, -> { active }, class_name: CourseTeacher, foreign_key: :course_id
  has_one :active_teacher, through: :active_course_teacher, source: :teacher

  validates :lesson_duration, :number_of_reschedule, :started_date, :time_zone, presence: true
  validate :overlap_teaching_schedules?, on: :create
  validate :avaiable_lesson_packages?, on: :create
  validate :overlap_weekly_schedules?, on: :create

  scope :order_by_id, -> { order(id: :desc) }
  scope :will_be_swapped, -> (length = 20) do
    join_sql = <<-SQL
      INNER JOIN (
        SELECT courses.id
        FROM courses
          INNER JOIN course_teachers ON courses.id = course_teachers.course_id
          INNER JOIN lesson_schedules ON courses.id = lesson_schedules.course_id
        WHERE
          courses.status = #{Course.statuses[:in_process]}
          AND courses.number_of_lessons > #{length}
          AND course_teachers.status = #{CourseTeacher.statuses[:active]}
          AND lesson_schedules.teacher_id = course_teachers.teacher_id
          AND lesson_schedules.started_time <= CURRENT_TIMESTAMP
          AND lesson_schedules.status IN (#{[:official, :insert].map { |s| LessonSchedule.statuses[s] }.join(',')})
        GROUP BY courses.id
        HAVING count(courses.id) >= 20
      ) as swapped_courses ON courses.id = swapped_courses.id
    SQL
    joins(join_sql)
  end

  accepts_nested_attributes_for :course_schedules, allow_destroy: true
  accepts_nested_attributes_for :course_teachers, allow_destroy: true

  def remain_of_lessons
    number_of_lessons - number_of_learned_lessons
  end

  def learned_lessons
    lesson_schedules.be_learned.where('ended_time <= ?', Time.current)
  end

  def number_of_learned_lessons
    learned_lessons.count
  end

  private

  def overlap_weekly_schedules?
    teacher = course_teachers.find { |ct| ct.active? }.teacher
    in_weekly = course_schedules.all? do |cs|
      teacher.weekly_schedules.exists? [
        'block_range @> int4range(?, ?) OR block_range @> int4range(?, ?)',
        cs.block_range_begin, cs.block_range_end,
        cs.block_range_begin + 336, cs.block_range_end + 336
      ]
    end

    if !in_weekly
      errors.add(:base, "Not available in #{teacher.full_name}'s weekly schedules")
    end
  end

  def overlap_teaching_schedules?
    teacher = course_teachers.find { |ct| ct.active? }.teacher
    in_teaching = Generator::LessonSchedulesService.new(self).course_date_ranges.any? do |rdate|
      teacher.lesson_schedules.be_learned.exists? [
        '(started_time >= :started_time AND started_time < :ended_time) OR (ended_time > :started_time AND ended_time <= :ended_time)',
        { started_time: rdate.first, ended_time: rdate.last }
      ]
    end

    if in_teaching
      errors.add(:base, "Overlap in teaching schedules")
    end
  end

  def avaiable_lesson_packages?
    if student.wallet.price < price
      errors.add(:base, 'User does not have enough money for this course')
    end
  end
end
