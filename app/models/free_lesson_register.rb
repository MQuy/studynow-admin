class FreeLessonRegister < ActiveRecord::Base
  PROGRAMS = [
    'English for Communication',
    'English in Office',
    'English for Children',
    'International Certification',
    'English on Demand'
  ]

  enum status: [:pending, :in_process, :success, :failure]

  validates :email, uniqueness: true
  validates :email, :full_name, :phone_number, presence: true
end
