class AddSlugToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :slug, :string
  end
end
