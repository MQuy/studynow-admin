class CourseMailerPreview < ActionMailer::Preview

  def estimation
    CourseMailer.estimation(Course.first.id)
  end
end
