class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :full_name, :nickname, :authentication_token, :skype, :timezone,
    :address, :avatar_url, :sign_in_count, :phone_number, :role, :slug, :status

  def avatar_url
    object.avatar.url
  end

  def role
    object.type.underscore
  end

  def timezone
    ActiveSupport::TimeZone::MAPPING[object.time_zone]
  end
end
