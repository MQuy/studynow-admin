class CourseMailer < ActionMailer::Base
  include Mjml::Mailer
  include Studynow::Mailer

  default from: 'Studynow <support@studynow.vn>'
  layout 'mailer'

  def estimation(course_id)
    course = Course.find(course_id)
    student = course.student
    teacher = course.active_course_teacher.teacher

    mjml(to: student.email, template_variables: {
      name: student.full_name,
      manager_name: teacher.manager.full_name
    })
  end
end
