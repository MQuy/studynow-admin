require 'grape/active_model_serializers'

module Partner
  class Route < Grape::API
    include Grape::ActiveModelSerializers

    mount Partner::V1::User
    mount Partner::V1::Report
  end
end
