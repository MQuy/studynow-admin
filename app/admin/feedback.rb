ActiveAdmin.register Feedback do

  actions :all, except: [:new, :edit]

  index do
    id_column
    column :email
    column :full_name
    column :phone_number
    column :content
    column :created_at
    column('Status') do |feedback|
      status_tag(feedback.status, class: admin_statuses[feedback.status])
    end
    actions do |feedback|
      if feedback.pending?
        link_to 'Process', in_process_feedback_path(feedback), method: :post
      elsif feedback.in_process?
        link_to 'Finish', complete_feedback_path(feedback), method: :post
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :email
      row :full_name
      row :phone_number
      row :content
      row('Status') do |feedback|
        status_tag(feedback.status, class: admin_statuses[feedback.status])
      end
      row :created_at
    end
    active_admin_comments
  end

  member_action :in_process, method: :post do
    if resource.in_process!
      redirect_to feedbacks_path, notice: "Feedback's status is changed into in process."
    else
      render action: :new
    end
  end

  member_action :complete, method: :post do
    if resource.completed!
      redirect_to feedbacks_path, notice: "Feedback's status is changed into completed."
    else
      render action: :new
    end
  end
end
