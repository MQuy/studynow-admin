module Client
  module V1
    class Uploader < Grape::API
      include Helper

      helpers do
        def render_data(uploader)
          uploader.success_action_status = '200'
          render({
            direct_fog_url: "https://#{ENV['AWS_BUCKET']}.s3.amazonaws.com/",
            key: uploader.key,
            acl: uploader.acl,
            success_action_status: uploader.success_action_status,
            policy: uploader.policy,
            algorithm: uploader.algorithm,
            credential: uploader.credential,
            date: uploader.date,
            signature: uploader.signature,
            max_size: 5
          }, serializer: EmptySerializer)
        end
      end

      resource :uploader do
        get '/avatar' do
          uploader = ::User.new.avatar
          render_data(uploader)
        end

        get '/attach_file' do
          uploader = ::AttachFile.new.path
          render_data(uploader)
        end
      end
    end
  end
end
