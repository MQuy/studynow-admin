Formtastic::Inputs::Base::Labelling.module_eval do
  def label_html_options
    { for: input_html_options[:id] }.merge(options[:label_html] || { class: ['label'] })
  end
end
