namespace :schema_rename do
  task :do => :environment do
    puts "Renaming schema_migrations table to schema_versions"
    ActiveRecord::Base.connection.execute('DROP TABLE schema_versions;')
    ActiveRecord::Base.connection.execute('ALTER TABLE schema_migrations RENAME TO schema_versions;')
    puts "Job complete..."
  end
end
