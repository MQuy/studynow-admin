class CreateLessonSchedule < ActiveRecord::Migration
  def change
    create_table :lesson_schedules do |t|
      t.belongs_to :course
      t.belongs_to :teacher
      t.belongs_to :student
      t.belongs_to :cause_by
      t.datetime :started_time
      t.datetime :ended_time
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
