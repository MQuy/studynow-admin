class PostSerializer < ActiveModel::Serializer
  has_one :author, serializer: AdminUserSerializer

  attributes :id, :title, :cover_image, :content, :markdown, :category, :tags, :brief, :created_at, :slug

  def cover_image
    object.cover_image.url
  end

  def category
    object.category.titleize
  end

  def tags
    object.tags.map { |tag| tag.titleize }
  end
end
