class UserArraySerializer < ActiveModel::Serializer
  attributes :id, :email, :full_name, :nickname, :skype, :avatar_url, :phone_number, :role, :slug

  def avatar_url
    object.avatar.url
  end

  def role
    object.type.underscore
  end
end
