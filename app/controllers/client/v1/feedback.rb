module Client
  module V1
    class Feedback < Grape::API
      include Helper

      helpers do
        def feedback_params
          ActionController::Parameters.new(params)
            .require(:feedback).permit(:full_name, :email, :phone_number, :content)
        end
      end

      resource :feedbacks do
        post do
          repository = FeedbackRepository.create(feedback_params)
          respond_with(repository)
        end
      end
    end
  end
end
