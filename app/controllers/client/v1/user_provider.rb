module Client
  module V1
    class UserProvider < Grape::API
      include Helper

      helpers do
        def provider_params
          ActionController::Parameters.new(params)
            .require(:user_provider).permit(:provider, :secret, :email, :uid, :token, :name, :timezone)
        end
      end

      resource :user_providers do
        post do
          repository = UserProviderRepository.create(provider_params)
          render repository.user_provider.user
        end
      end
    end
  end
end
