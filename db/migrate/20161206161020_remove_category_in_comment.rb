class RemoveCategoryInComment < ActiveRecord::Migration
  def change
    remove_column :comments, :category, :string
  end
end
