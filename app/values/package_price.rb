class PackagePrice

  class << self
    def get(category, number_of_lessons, currency = :vnd)
      factor = send("factor_#{category}", number_of_lessons)
      Money.new(factor * number_of_lessons, :vnd).exchange_to(currency)
    end

    def factor_half_hour(numbers)
      case numbers
      when 1..9 then 120_000
      when 10..19 then 110_000
      else 100_000
      end
    end

    def factor_one_hour(numbers)
      case numbers
      when 1..9 then 200_000
      when 10..19 then 180_000
      else 165_000
      end
    end
  end

end
