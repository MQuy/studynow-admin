class Wallet < ActiveRecord::Base

  monetize :price_units

  belongs_to :user
  has_many :transactions, as: :payable
end
