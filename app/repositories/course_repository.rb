class CourseRepository
  extend Helpers::RepoAttr

  attr_record :course

  class << self
    def find(id)
      course = id.to_s =~ /\A\d+\z/ ? Course.find(id) : Course.find_by(code: id)
      load course
    end

    def create(args)
      repo = new(args)
      course = repo.course

      course.course_schedules = course.course_schedules.map do |ws|
        CourseScheduleRepository.load(ws, course.lesson_duration, course.time_zone).course_schedule
      end
      if course.price_units.blank?
        course.price = Generator::LessonSchedulesService.new(course)
                                                          .course_package_prices
                                                          .inject(:+)
      end
      ActiveRecord::Base.transaction do
        if course.save!
          Generator::LessonSchedulesService.new(course).execute
          StudentRepository.load(course.student).deposit_course(course)
          repo.transfer_referral_student
        end
      end
    ensure
      return repo
    end
  end

  def initialize(course_params = {})
    @course = Course.new(course_params)
  end

  def destroy
    ActiveRecord::Base.transaction do
      student_wallet = course.student.wallet
      if course.destroy!
        WalletRepository.load(student_wallet)
          .receive(course.price, :course_delete, { course_id: course.id })
      end
    end
  end

  def complete
    return if course.completed?
    ActiveRecord::Base.transaction do
      course.completed!
    end
  end

  def cancel
    return if course.cancel?
    ActiveRecord::Base.transaction do
      if course.cancel!
        teaching_price = course.lesson_schedules.be_learned.map(&:price).inject(:+)
        remain_price = course.price - teaching_price
        WalletRepository.load(course.student.wallet).receive(remain_price, :course_refund, { course_id: course.id })
      end
    end
  end

  def transfer_referral_student
    referrer = course.student.referrer
    return unless referrer&.student?

    referral_price = course.price * StudentRepository.load(referrer).referral_ratio
    WalletRepository.load(referrer.wallet).receive(referral_price, :course_referrer, { course_id: course.id })
  end

  def build_lesson_schedule(args)
    course.lesson_schedules.build(
      teacher_id: course.active_teacher.id,
      student_id: course.student_id,
      price: args[:price],
      status: args[:status] || :official,
      started_time: args[:started_time],
      ended_time: args[:ended_time]
    )
  end
end
