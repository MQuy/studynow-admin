class ChangeCodeToUser < ActiveRecord::Migration
  def change
    change_column :users, :code, :string
  end
end
