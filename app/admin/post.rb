ActiveAdmin.register Post do
  permit_params :author_id, :title, :markdown, :content, :category, :brief, :cover_image, tags: []

  filter :author
  filter :title
  filter :slug

  index do
    id_column
    column :author
    column :title
    column(:category) { |post| post.category.titleize }
    column(:tags) { |post| post.tags.map(&:titleize).join(', ') }
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :author
      row :category
      row :title
      row :cover_image
      row :brief
      row :markdown
      row(:tags) { |post| post.tags.map(&:titleize).join(', ') }
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form partial: 'admin/posts/form'

  controller do
    def create
      params[:post][:tags] = params[:post][:tags].split(',')
      repo = PostRepository.create(permitted_params[:post])
      @post = repo.post

      if repo.persisted?
        redirect_to post_path(repo.id), notice: "Post is created successfully."
      else
        render action: :new
      end
    end

    def update
      params[:post][:tags] = params[:post][:tags].split(',')
      update!
    end
  end
end
