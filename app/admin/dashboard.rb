ActiveAdmin.register_page "Dashboard", as: :dashboard do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  action_item :statistics_teacher do
    link_to "Teacher Statistics", dashboard_teacher_path
  end

  action_item :statistics_student do
    link_to "Student Statistics", dashboard_student_path
  end

  content do
    time_zone = params[:time_zone] || 'Hanoi'
    in_date = ActiveSupport::TimeZone[time_zone].parse(params[:in_date] || Date.today.to_s)
    lesson_schedules = LessonSchedule
                                     .includes(:teacher, :student, :course)
                                     .where(teacher_id: Pundit.policy_scope(current_admin_user, Teacher).pluck(:id))
                                     .where("tsrange(started_time, ended_time, '[]') && tsrange(?, ?, '[]')", (in_date - 1.day).beginning_of_day, (in_date + 1.day).end_of_day)

    dates = { previous: [], current: [], next: [] }
    lesson_schedules.find_each do |ls|
      range = (ls.started_time - in_date) / (3600 * 24.0)
      anchor = range < 0 ? :previous : (range >= 1 ? :next: :current)
      dates[anchor] << ls
    end

    dates.each { |key, value| value.sort_by! { |x| [x.teacher_id, x.student_id, x.started_time] } }

    render 'admin/dashboard/index', dates: dates, in_date: in_date, time_zone: time_zone
  end

  page_action :teacher, method: :get do
    statistic = OpenStruct.new({ teacher_id: params[:teacher_id], started_date: params[:started_date], ended_date: params[:ended_date] })
    locals = { statistic: statistic, state: 'empty' }

    if params[:teacher_id].present?
      time_zone = params[:time_zone] || 'Hanoi'
      started_date = ActiveSupport::TimeZone[time_zone].parse(params[:started_date]).beginning_of_day.iso8601
      ended_date = ActiveSupport::TimeZone[time_zone].parse(params[:ended_date]).end_of_day.iso8601

      teacher_proc = Proc.new do |teacher, started_date, ended_date|
        lesson_schedules = SchedulesInRangeService.new(teacher, { started_time: started_date, ended_time: ended_date }).execute
        lesson_schedules = lesson_schedules.order(:started_time)
        total_hours = lesson_schedules.all.map(&:length).inject(:+).to_f / 60
        total_students = lesson_schedules.pluck(:student_id).uniq.length
        [lesson_schedules, total_students, total_hours]
      end

      if params[:teacher_id] == 'all'
        teachers = Pundit.policy_scope(current_admin_user, Teacher).map do |teacher|
          lesson_schedules, total_students, total_hours = teacher_proc.call(teacher, started_date, ended_date)
          [teacher, lesson_schedules, total_students, total_hours]
        end
        locals = locals.merge({ teachers: teachers, state: 'collection' })
      else
        teacher = Teacher.find(params[:teacher_id])
        lesson_schedules, total_students, total_hours = teacher_proc.call(teacher, started_date, ended_date)
        locals = locals.merge({ lesson_schedules: lesson_schedules, total_students: total_students, total_hours: total_hours, state: 'single' })
      end
    end

    render 'admin/dashboard/teacher', locals: locals
  end

  page_action :student, method: :get do
    statistic = OpenStruct.new({ student_id: params[:student_id], started_date: params[:started_date], ended_date: params[:ended_date] })
    locals = { statistic: statistic, state: 'empty' }

    if params[:student_id].present?
      if params[:student_id] == 'all'
        time_zone = params[:time_zone] || 'Hanoi'
        started_date = ActiveSupport::TimeZone[time_zone].parse(params[:started_date]).beginning_of_day.iso8601
        ended_date = ActiveSupport::TimeZone[time_zone].parse(params[:ended_date]).end_of_day.iso8601
        students = Pundit.policy_scope(current_admin_user, Student).map do |student|
          lesson_schedules = SchedulesInRangeService.new(student, { started_time: started_date, ended_time: ended_date }).execute
          [student, lesson_schedules]
        end
        locals = locals.merge({ students: students, state: 'collection' })
      else
        student = Student.find(params[:student_id])
        locals = locals.merge({ student: student, state: 'single' })
      end
    end
    render 'admin/dashboard/student', locals: locals
  end
end
