class SchedulesInRangeService
  attr_accessor :current_user, :started_time, :ended_time, :params

  def initialize(user, params)
    @current_user, @params  = user, params
    @started_time = Time.parse(params[:started_time])
    @ended_time = Time.parse(params[:ended_time])
  end

  def execute
    filter_by_status
    restrict_in_range
    apply_only_partner
    schedules
  end

  def schedules
    @schedules ||= current_user.lesson_schedules.be_learned.includes(:course, :student, :teacher)
  end

  def filter_by_status
    @schedules = if params[:statuses].blank?
      schedules.joins(:course).where(courses: { status: Course.statuses.values })
    else
      schedules.joins(:course).where(courses: { status: params[:statuses].map { |s| Course.statuses[s] } })
    end
  end

  def restrict_in_range
    @schedules = schedules.where("tsrange(started_time, ended_time, '[]') && tsrange(?, ?, '[]')", started_time, ended_time)
  end

  def apply_only_partner
    return if partner_id.blank?

    @schedules = if current_user.student?
      schedules.where(teacher_id: partner_id)
    else
      schedules.where(student_id: partner_id)
    end
  end

  def partner_id
    @partner_id ||= if params[:partner_id].present?
      User.find(params[:partner_id]).id
    end
  end
end
