class CreateComment < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :commentable, polymorphic: true
      t.belongs_to :user
      t.text :content

      t.timestamps
    end
  end
end
