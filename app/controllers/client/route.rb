require 'grape/active_model_serializers'

module Client
  class Route < Grape::API
    include Grape::ActiveModelSerializers

    mount Client::V1::User
    mount Client::V1::UserProvider
    mount Client::V1::Teacher
    mount Client::V1::Student
    mount Client::V1::WeeklySchedule
    mount Client::V1::LessonSchedule
    mount Client::V1::Course
    mount Client::V1::Comment
    mount Client::V1::CourseNote
    mount Client::V1::AttachFile
    mount Client::V1::Uploader
    mount Client::V1::FreeLessonRegister
    mount Client::V1::Feedback
    mount Client::V1::Post
    mount Client::V1::Notification
    mount Client::V1::HomeworkStudent
  end
end
