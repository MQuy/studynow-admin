require 'sidekiq/api'

namespace :sidekiq do
  task clear: :environment do
    Sidekiq::Queue.new.clear
    Sidekiq::ScheduledSet.new.clear
    Sidekiq::RetrySet.new.clear

    Sidekiq.redis { |conn| conn.flushdb }
  end
end
