class AddNumberOfRescheduleToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :number_of_reschedule, :integer, default: 4
  end
end
