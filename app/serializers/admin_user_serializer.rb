class AdminUserSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :slug
end
