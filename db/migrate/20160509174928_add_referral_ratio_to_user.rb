class AddReferralRatioToUser < ActiveRecord::Migration
  def change
    add_column :users, :referral_ratio, :decimal, precision: 4, scale: 2
  end
end
