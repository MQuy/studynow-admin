class ChartsController < ApplicationController

  def student_fees
    student_ids = Student.pluck(:id)
    student_fees = Graph::StudentFee.new(student_ids).execute

    render json: student_fees
  end
end
