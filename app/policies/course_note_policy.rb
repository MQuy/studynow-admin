class CourseNotePolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def index?
    user.admin? || user.manager?
  end

  def show?
    user.admin? || user.manager?
  end
end
