module Client
  module V1
    class Course < Grape::API
      include Helper

      helpers do
        def repository
          CourseRepository.find(params[:id])
        end
      end

      resource :courses do
        get ':id' do
          respond_with(repository)
        end

        get ':id/teacher_schedules' do
          lesson_schedules = SchedulesInRangeService.new(repository.course.active_teacher, params).execute
          render lesson_schedules, each_serializer: LessonScheduleSerializer
        end
      end
    end
  end
end
