class CommentSerializer < ActiveModel::Serializer
  has_one :attach_file

  attributes :user_id, :content, :commentable_id, :commentable_type, :created_at, :user

  def user
    if object.user
      UserSerializer.new(object.user)
    else
      {
        nickname: 'System',
        avatar_url: 'https://s3-ap-southeast-1.amazonaws.com/studynow/public/default_avatar.jpg'
      }
    end
  end
end
