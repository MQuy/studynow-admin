class CourseTeacher < ActiveRecord::Base

  enum status: [:active, :expired]

  belongs_to :teacher
  belongs_to :course

  before_create :ensure_assigned_at

  validate :overlap_teaching_schedules?, on: :create, if: -> { course.present? }

  private

  def ensure_assigned_at
    self.assigned_at ||= Time.current
  end

  def overlap_teaching_schedules?
    in_teaching = course.lesson_schedules.be_learned.where("started_time >= ? AND ended_time <= ?", assigned_at, expired_at || 100.years.from_now).any? do |ls|
      teacher.lesson_schedules.be_learned.exists? [
        '(started_time >= :started_time AND started_time < :ended_time) OR (ended_time > :started_time AND ended_time <= :ended_time)',
        { started_time: ls.started_time, ended_time: ls.ended_time }
      ]
    end

    if in_teaching
      errors.add(:base, "Overlap in teaching schedules")
    end
  end
end
