class CommentSearchService
  attr_accessor :current_user, :commentable_type, :commentable_id

  def initialize(user, commentable_type:, commentable_id:)
    @current_user = user
    @commentable_type = commentable_type
    @commentable_id = commentable_id
  end

  def execute
    object.comments.order(id: :desc)
  end

  def object
    @object ||= if commentable_id =~ /\A\d+\z/
      klass.find(commentable_id)
    else
      klass.find_by(code: commentable_id)
    end
  end

  def klass
    @klass ||= commentable_type.constantize
  end
end
