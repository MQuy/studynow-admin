class CreateCourseSchedule < ActiveRecord::Migration
  def change
    create_table :course_schedules do |t|
      t.belongs_to :course
      t.int4range :block_range

      t.timestamps
    end
  end
end
