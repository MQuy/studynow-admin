class UserProvider < ActiveRecord::Base
  enum provider: [:facebook, :google, :twitter]

  belongs_to :user

  validates :provider, uniqueness: { scope: :user_id }, presence: true
  validates :token, presence: true
end
