class CreateHomeworkStudent < ActiveRecord::Migration
  def change
    create_table :homework_students do |t|
      t.belongs_to :homework
      t.belongs_to :student

      t.timestamps
    end
  end
end
