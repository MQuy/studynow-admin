class AddReferralCodeToUser < ActiveRecord::Migration
  def change
    add_column :users, :referral_code, :integer
  end
end
