module PathHelper
  module_function

  def post_url(post)
    domain + "/posts/" + post.slug
  end

  def domain
    ENV['DOMAIN']
  end
end
