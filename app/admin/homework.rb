ActiveAdmin.register Homework do
  permit_params :admin_user_id, :content, attach_file_attributes: [:id, :path]

  action_item :homework_students, only: :show do
    link_to('Students', homework_homework_students_path(homework_id: homework.id))
  end

  filter :admin_user
  filter :content
  filter :code

  index do
    selectable_column
    id_column
    column :admin_user
    column :content
    column('Attach file') do |homework|
      if homework.attach_file.present?
        link_to 'View', homework.attach_file.path.url
      end
    end
    column :created_at
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Homework' do
      f.input :admin_user_id, as: :hidden, input_html: { value: current_admin_user.id }
      f.input :content
    end
    if f.object.attach_file.blank?
      f.object.build_attach_file
    end
    f.inputs "Attach file", for: [:attach_file, f.object.attach_file] do |at|
      at.input :path, as: :file, :hint => Proc.new {
        if f.object.attach_file.path.url.present?
          link_to(f.object.attach_file.path.url, f.object.attach_file.path.url, download: true).html_safe
        else
          ''
        end
      }.call
    end
    f.actions
  end

  controller do
    def create
      repo = HomeworkRepository.create(permitted_params[:homework])
      @homework = repo.homework

      if repo.persisted?
        redirect_to homework_path(repo.id), notice: "Homework is created successfully."
      else
        render action: :new
      end
    end
  end
end
