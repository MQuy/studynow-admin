ActiveAdmin.register Comment, as: 'HomeworkStudentComment' do
  belongs_to :homework_student
  actions :all, except: [:edit]
  permit_params :content, attach_file_attributes: [:id, :path]

  index do
    selectable_column
    id_column
    column('User') do |comment|
      if comment.user.present?
        link_to comment.user.full_name, user_path(comment.user.id)
      else
        'System'
      end
    end
    column :content
    column('Attachment') do |comment|
      if comment.attach_file
        link_to 'Download', comment.attach_file.path.url, download: true
      end
    end
    column :created_at
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Comment' do
      f.input :content
    end
    if f.object.attach_file.blank?
      f.object.build_attach_file
    end
    f.inputs "Attach file", for: [:attach_file, f.object.attach_file] do |at|
      at.input :path, as: :file, :hint => Proc.new {
        if f.object.attach_file.path.url.present?
          link_to(f.object.attach_file.path.url, f.object.attach_file.path.url, download: true).html_safe
        else
          ''
        end
      }.call
    end
    f.actions
  end
end
