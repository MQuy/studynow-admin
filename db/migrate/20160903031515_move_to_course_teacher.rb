class MoveToCourseTeacher < ActiveRecord::Migration
  def up
    Course.find_each do |course|
      course_teacher = course.course_teachers.new(
        assigned_at: course.started_date,
        teacher_id: course.teacher_id,
        status: :active
      )
      course_teacher.save!(validate: false)
    end
    remove_column :courses, :teacher_id
  end

  def down; end
end
