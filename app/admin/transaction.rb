ActiveAdmin.register Transaction do
  belongs_to :user

  index do
    id_column
    column :payable
    column('Price') { |transaction| money_format(transaction.price) }
    column('Category') { |transaction| transaction.category.titleize }
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :payable
      row('Price') { |transaction| money_format(transaction.price) }
      row('Category') { |transaction| transaction.category.titleize }
      row :data
      row :created_at
    end
    active_admin_comments
  end
end
