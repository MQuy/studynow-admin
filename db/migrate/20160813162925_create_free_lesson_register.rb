class CreateFreeLessonRegister < ActiveRecord::Migration
  def change
    create_table :free_lesson_registers do |t|
      t.string :full_name
      t.string :phone_number
      t.string :email
      t.string :skype

      t.timestamps
    end
  end
end
