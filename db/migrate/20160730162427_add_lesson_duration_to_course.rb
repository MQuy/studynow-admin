class AddLessonDurationToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :lesson_duration, :integer
  end
end
