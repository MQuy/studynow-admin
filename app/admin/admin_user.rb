ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :role, :full_name, teacher_ids: []
  filter :email
  filter :full_name

  index do
    id_column
    column :email
    column :full_name
    column('Role') do |admin|
      status_tag(admin.role, class: admin_statuses[admin.role])
    end
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :full_name
      row('Role') do |admin|
        status_tag(admin.role, class: admin_statuses[admin.role])
      end
      row :current_sign_in_at
      row :created_at
    end
    if resource.manager?
      panel 'List of teachers' do
        attributes_table_for resource do
          resource.teacher_ids.each.with_index do |teacher_id, index|
            row("Teacher #{index + 1}") { Teacher.find(teacher_id) }
          end
        end
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    if f.object.persisted?
      render 'admin/admin_users/edit', context: self, f: f
    else
      render 'admin/admin_users/new', context: self, f: f
    end
    f.actions
  end

end
