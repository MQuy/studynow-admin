class NotificationsResolve < BaseResolve
  attr_accessor :notifications

  def execute
    filter_user if args[:user_id].present?
    filter_page if args[:page].present?
    notifications
  end

  private

  def filter_user
    @notifications = notifications.where(user_id: args[:user_id])
  end

  def filter_page
    @notifications = notifications.offset(args[:page].to_i * 10)
  end

  def notifications
    @notifications ||= Notification.order(created_at: :desc)
                                   .limit(10)
  end
end
