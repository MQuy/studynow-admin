module Helpers
  module EditorPolicy
    def index?
      user.editor?
    end

    def show?
      user.editor?
    end

    def update?
      user.editor?
    end

    def create?
      user.editor?
    end

    def destroy?
      user.editor?
    end
  end
end
