module Client
  module V1
    class HomeworkStudent < Grape::API
      include Helper

      helpers do
        def repository
          HomeworkStudentRepository.find(params[:id])
        end
      end

      resource :homework_students do
        get do
          render current_user.homework_students, each_serializer: HomeworkStudentSerializer
        end

        get ':id' do
          respond_with(repository)
        end
      end
    end
  end
end
