class Comment < ActiveRecord::Base

  belongs_to :commentable, polymorphic: true
  belongs_to :user
  has_one :attach_file, as: :attachable
  has_many :notifications, as: :notificable

  accepts_nested_attributes_for :attach_file, allow_destroy: true
end
