module Client
  module V1
    class Comment < Grape::API
      include Helper

      helpers do
        def comment_params
          ActionController::Parameters.new(params)
            .require(:comment).permit(:commentable_type, :commentable_id, :user_id, :content)
        end

        def attach_file_params
          ActionController::Parameters.new(params)
            .require(:attach_file).permit!
        end
      end

      resource :comments do
        get do
          comments = CommentSearchService.new(current_user, params.to_hash(symbolize_keys: true)).execute
          render comments, each_serializer: CommentSerializer
        end

        post do
          repository = CommentRepository.create(comment_params)
          if repository.persisted? && params[:attach_file].present?
            AttachFileRepository.create({ user_id: current_user.id, attachable_id: repository.id, attachable_type: "Comment", remote_path_tmp_url: params[:attach_file][:url] })
          end
          respond_with(repository)
        end
      end
    end
  end
end
