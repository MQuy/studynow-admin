class ShiftRangeOnDistance

  def initialize(block_range)
    @block_range_begin = block_range&.first
    @block_range_end = block_range&.last
  end

  def execute(distance)
    shift_on_distance(distance) if block_range_begin && block_range_end
    block_range
  end

  def block_range
    block_range_begin..block_range_end
  end

  private

  attr_accessor :block_range_begin, :block_range_end

  def shift_on_distance(distance)
    @block_range_begin = block_range_begin - distance
    @block_range_end = block_range_end - distance

    if block_range_begin < 0
      @block_range_begin += 336
    end

    if block_range_end < 0
      @block_range_end += 336
    end

    if block_range_begin > block_range_end
      @block_range_end += 336
    end
  end
end

