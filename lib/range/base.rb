module Range::Base
  def range_assign(*attrs)
    return unless table_exists?

    attrs.each do |attr|
      define_method "#{attr}_length" do
        length = send("#{attr}_end") - send("#{attr}_begin")
        length * 30
      end

      define_method "#{attr}_length=" do |number|
        range_begin = send("#{attr}_begin")
        range_end = range_begin + (number.to_f / 30).ceil
        send("#{attr}=", range_begin..range_end)
      end
    end

    attrs.product([:begin, :end]).each do |attr, suffix|
      column_type = columns_hash[attr.to_s].type.to_s.titleize
      klass = "Range::#{column_type}::Attributes".constantize

      define_method "#{attr}_#{suffix}" do
        klass.new(self, attr, nil, suffix).boundary
      end

      define_method "#{attr}_#{suffix}=" do |value|
        send("#{attr}=", klass.new(self, attr, nil, suffix).empty_range)
        send("#{attr}_day_#{suffix}=", value / 48)
        send("#{attr}_time_#{suffix}=", "#{value % 48 / 2}:#{(value % 2) * 30}")
      end
    end

    attrs.product([:day, :time], [:begin, :end]).each do |attr, type, suffix|
      column_type = columns_hash[attr.to_s].type.to_s.titleize
      klass = "Range::#{column_type}::Attributes".constantize

      define_method "#{attr}_#{type}_#{suffix}" do
        klass.new(self, attr, type, suffix).fragment
      end

      define_method "#{attr}_#{type}_#{suffix}=" do |value|
        object = klass.new(self, attr, type, suffix)
        object.assign(value)
        send("#{attr}=", object.range)
      end
    end
  end
end
