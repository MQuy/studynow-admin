ActiveAdmin.register FreeLessonRegister do

  actions :all, except: [:create, :new]
  permit_params :full_name, :phone_number, :email, :skype, :programs, :status

  index do
    id_column
    column :email
    column :full_name
    column :phone_number
    column :skype
    column('Programs') do |lesson|
      lesson.programs.map { |program| FreeLessonRegister::PROGRAMS[program.to_i] }.join(', ')
    end
    column('Status') do |lesson|
      status_tag(lesson.status, class: admin_statuses[lesson.status])
    end
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :full_name
      row :phone_number
      row :skype
      row('Status') do |lesson|
        status_tag(lesson.status, class: admin_statuses[lesson.status])
      end
      row('Programs') do |lesson|
        lesson.programs.map { |program| FreeLessonRegister::PROGRAMS[program.to_i] }.join(', ')
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Free Lesson Register Details' do
      f.input :status, as: :select, collection: free_lesson_opts, include_blank: false, selected: f.object.status || 'Pending'
    end
    f.actions
  end
end
