//= require active_admin/base
//= require jquery-ui
//= require jquery-ui-timepicker-addon
//= require highcharts
//= require highcharts.exporting
//= require picker
//= require simplemde.min
//= require tokenizer

$(document).ready(function() {
  setupPicker();
});
