class LessonScheduleSerializer < ActiveModel::Serializer
  attributes :id, :started_time, :ended_time, :status, :course_code, :teacher_name, :student_name, :teacher_avatar, :student_avatar, :student_id, :teacher_id, :course_id, :duration

  def course_code
    object.course.code
  end

  def teacher_name
    object.teacher.full_name
  end

  def student_name
    object.student.nickname
  end

  def teacher_avatar
    object.teacher.avatar.url
  end

  def student_avatar
    object.student.avatar.url
  end

  def duration
    object.length
  end
end
