module Client
  module V1
    class Student < Grape::API
      include Helper

      helpers do
        def repository
          StudentRepository.find(params[:id])
        end
      end

      resource :students do
        get ':id' do
          respond_with(repository)
        end

        get ':id/teachers' do
          render repository.student.active_teachers, each_serializer: TeacherSerializer
        end
      end
    end
  end
end
