module Studynow
  module Mailer
    def mjml(headers, &block)
      email = headers[:to]
      user = User.find_by(email: email)
      locale = user&.student? ? :vi : :en

      I18n.with_locale(locale) do
        super
      end
    end
  end
end
