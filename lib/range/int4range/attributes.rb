module Range::Int4range
  class Attributes
    attr_accessor :object, :type, :suffix, :attr

    def initialize(object, attr, type, suffix)
      @object = object
      @attr = attr
      @type = type
      @suffix = suffix
    end

    def assign(v)
      @boundary = if type == :day
        v.to_i * 48 + boundary / 48
      elsif type == :time
        hour, min = v.split(':').map { |x| x.to_i }
        boundary / 48 * 48 + (hour * 2 + min / 30)
      end
    end

    def boundary
      @boundary ||= suffix == :end ? object_attr_value_end : object_attr_value_begin
    end

    def fragment
      type == :day ? boundary / 48 % 7 : boundary % 48
    end

    def range
      suffix == :end ? object_attr_value_begin..boundary : boundary..object_attr_value_end
    end

    def empty_range
      suffix == :end ? object_attr_value_begin..0 : 0..object_attr_value_end
    end

    def decorator
      Decorator.new(self)
    end

    private

    def object_attr_value
      object.send(attr)
    end

    def object_attr_value_begin
      object_attr_value.try(:begin) || 0
    end

    def object_attr_value_end
      object_attr_value.try(:exclude_end?) ? object_attr_value.try(:end) - 1 : object_attr_value.try(:end) || 0
    end
  end
end
