module Partner
  module V1
    class Report < Grape::API
      include Helper

      resource :report do
        get :users do
          if current_user
            student_ids = current_user.referral_users.pluck(:id)
            student_fees = Graph::StudentFee.new(student_ids)
            render({ values: student_fees.values, labels: student_fees.labels }, serializer: EmptySerializer)
          else
            error!('401 Unauthorized', 401)
          end
        end

        get 'users/:id' do
          if current_user
            student = Student.find(params[:id])
            student_fees = Graph::StudentFee.new([student.id])
            render({ values: student_fees.values[student.id], labels: student_fees.labels }, serializer: EmptySerializer)
          else
            error!('401 Unauthorized', 401)
          end
        end
      end
    end
  end
end
