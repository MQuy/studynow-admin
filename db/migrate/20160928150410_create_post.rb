class CreatePost < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.belongs_to :author
      t.string :title
      t.string :cover_image
      t.text :content
      t.text :markdown
      t.integer :category
      t.string :slug
      t.string :brief
      t.text :tags, array: true, default: []

      t.timestamps
    end
  end
end
