module Generator
  class LessonSchedulesService
    PACKAGES = [:half_hour, :one_hour]

    attr_accessor :course, :started_date, :time_zone

    def initialize(course)
      @course = course
      @started_date = course.started_date.in_time_zone(course.time_zone)
    end

    def execute
      lesson_schedules = []
      course_date_ranges.each do |rdate|
        package = (rdate.last - rdate.first) / 30.minutes - 1
        lesson_schedules << CourseRepository.load(course).build_lesson_schedule({
          started_time: rdate.first,
          ended_time: rdate.last,
          price: course.price / course.number_of_lessons
        })
      end

      ::LessonSchedule.import! lesson_schedules
    end

    def course_date_ranges
      course_wday_ranges.map do |wday|
        started_time = started_date + (wday.first * 30).minute
        ended_time = started_date + (wday.last * 30).minute

        started_time..ended_time
      end
    end

    def course_full_weeks
      (1..number_of_weeks).inject(course_week) do |result, index|
        upcoming_week = course_week.map { |wday| (wday.first + 336 * index)..(wday.last + 336 * index) }
        result + upcoming_week
      end
    end

    def number_of_weeks
      (course.number_of_lessons.to_f / course.course_schedules.size).ceil + 1
    end

    def course_week
      distance = Time.now.in_time_zone(course.time_zone).utc_offset / 3600 * 2
      @course_week ||=
        course
          .course_schedules.to_a
          .map { |cs| ShiftRangeOnDistance.new(cs.block_range_begin..cs.block_range_end).execute(-distance) }
    end

    def course_wday_ranges
      started_wday = started_date.wday
      course_full_weeks
        .map { |wday| (wday.first - started_wday * 48)..(wday.last - started_wday * 48) }
        .select { |wday| wday.begin >= 0 }
        .slice(0, course.number_of_lessons)
    end

    def course_packages
      @course_packages ||= course_wday_ranges.inject([0, 0]) do |results, wday|
        range = wday.last - wday.first
        results[range - 1] += 1 and results
      end
    end

    def course_package_prices
      @course_package_prices ||= course_packages.map.with_index { |p, i| PackagePrice.get(PACKAGES[i], p) }
    end
  end
end
