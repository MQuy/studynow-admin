class UserMailerPreview < ActionMailer::Preview

  def welcome
    UserMailer.welcome(Student.first.id)
  end

  def reset_password
    UserMailer.reset_password(Student.first.id)
  end
end
