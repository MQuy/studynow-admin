class ReferralUserSerializer < ActiveModel::Serializer
  attributes :id, :email, :full_name, :phone_number, :address, :referral_price, :currency, :avatar_url

  def referral_price
    object.referral_price || 0
  end
end

