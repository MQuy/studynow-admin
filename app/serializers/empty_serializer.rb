class EmptySerializer < ActiveModel::Serializer

  def attributes(fields=[])
    object.as_json
  end
end
