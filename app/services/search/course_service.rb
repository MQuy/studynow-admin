module Search
  class CourseService
    attr_accessor :courses, :partner, :params

    def initialize(partner, params = {})
      @partner = partner
      @params = params
    end

    def execute
      filter_by_status
      courses
    end

    private

    def courses
      @courses ||= if params[:active].present?
        partner.active_courses
      else
        partner.courses
      end
    end

    def filter_by_status
      return if params[:statuses].blank?

      @courses = courses.where(status: params[:statuses].map { |s| Course.statuses[s] })
    end
  end
end
