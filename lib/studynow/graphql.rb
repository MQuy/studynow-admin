GraphQL::RAW_TYPE = GraphQL::ScalarType.define do
  name "Raw"
  description "Represents json values."

  coerce_input ->(value) { value }
  coerce_result ->(value) { value }
end
