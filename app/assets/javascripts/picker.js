function setupPicker(tagRoot) {
  tagRoot = tagRoot || $('#wrapper');

  tagRoot.find('.datetime-picker').datetimepicker({
    dateFormat: "dd/mm/yy",
    timeFormat: 'HH:mm z',
    stepMinute: 30,
    beforeShow: function () {
      setTimeout(
        function () {
          $('#ui-datepicker-div').css("z-index", "3000");
        }, 100
      );
    }
  });

  tagRoot.find('.time-picker').timepicker({ stepMinute: 30 });

  tagRoot.find('.time-picker').each(function(i, ele) {
    var time, format;

    time = +$(ele).val();
    if (!isNaN(time)) {
      format = numberPad(Math.floor(time / 2), 2) + ":" + numberPad((time % 2) * 30, 2);
      $(ele).val(format);
    }
  });

  function numberPad(n, b) {
    return (1e15 + n + '').slice(-b);
  }
}
