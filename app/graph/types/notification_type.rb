NotificationType = GraphQL::ObjectType.define do
  name "Notification"
  description "A notification for user"

  field :id, types.ID
  field :user, UserType
  field :action, types.String do
    resolve -> (obj, args, ctx) { obj.action }
  end
  field :notificable_type, types.String do
    resolve -> (obj, args, ctx) { obj.notificable_type }
  end
  field :read, types.Boolean
  field :creator, UserType
  field :data, GraphQL::RAW_TYPE
  field :created_at, types.String do
    resolve -> (obj, args, ctx) { obj.created_at.iso8601 }
  end
end
