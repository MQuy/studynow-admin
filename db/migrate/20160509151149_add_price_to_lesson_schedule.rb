class AddPriceToLessonSchedule < ActiveRecord::Migration
  def change
    add_monetize :lesson_schedules, :price
  end
end
