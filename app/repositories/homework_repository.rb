class HomeworkRepository
  extend Helpers::RepoAttr

  attr_record :homework

  class << self
    def create(args)
      repo = new(args)
      homework = repo.homework
      ActiveRecord::Base.transaction do
        homework.save!
        homework_students = Student.pluck(:id).inject([]) do |acc, elem|
          acc << homework.homework_students.new(student_id: elem) and acc
        end
        HomeworkStudent.import! homework_students
      end
    ensure
      return repo
    end
  end

  def initialize(homework_params = {})
    @homework = Homework.new(homework_params)
  end
end
