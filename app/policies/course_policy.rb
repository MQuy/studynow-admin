class CoursePolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def index?
    user.admin? || user.manager?
  end

  def show?
    user.admin? || user.manager?
  end

  def complete?
    user.superadmin?
  end

  def cancel?
    user.admin?
  end

  def destroy?
    user.superadmin?
  end

  class Scope < Scope

    def resolve
      if user.admin?
        scope
      else
        scope.joins(:active_course_teacher).where(course_teachers: { teacher_id: user.teacher_ids })
      end
    end
  end
end
