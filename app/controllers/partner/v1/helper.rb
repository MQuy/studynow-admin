module Partner
  module V1
    module Helper
      def self.included(base)
        base.class_eval do

          version 'v1'
          format :json

          helpers do
            def authenticate!
              error!('401 Unauthorized', 401) unless current_user
            end

            def warden
              env['warden']
            end

            def authenticated
              return true if warden.authenticated?
              @user ||= ::User.find_by(email: headers['Email']&.downcase, authentication_token: headers['Token'])
            end

            def current_user
              authenticated
              warden.user || @user
            end

            def show_errors(model)
              model.errors.map { |k, v| v }
            end

            def respond_with(record, action = :valid?)
              if record.send(action)
                render record
              else
                render({ errors: record.errors.to_a }, serializer: EmptySerializer)
              end
            end
          end
        end
      end
    end
  end
end
