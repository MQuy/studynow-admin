class FreeLessonRegisterRepository
  extend Helpers::RepoAttr

  attr_record :free_lesson_register

  def initialize(free_lesson_register_params = {})
    @free_lesson_register = FreeLessonRegister.new(free_lesson_register_params)
  end
end
