class UserMailer < ActionMailer::Base
  include Mjml::Mailer
  include Studynow::Mailer

  default from: 'Studynow <support@studynow.vn>'
  layout 'mailer'

  def welcome(user_id)
    user = User.find(user_id)

    mjml(to: user.email, template_variables: {
      name: user.full_name
    })
  end

  def reset_password(user_id)
    user = User.find(user_id)

    mjml(to: user.email, template_variables: {
      name: user.full_name,
      token: user.authentication_token
    })
  end
end

