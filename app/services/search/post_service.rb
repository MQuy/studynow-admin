module Search
  class PostService
    attr_accessor :posts, :params

    def initialize(params)
      @params = params
      @posts = Post.joins(:author)
                   .includes(:author)
                   .order(created_at: :desc)
                   .limit(10)
    end

    def execute
      filter_category
      filter_tag
      filter_author
      filter_page
      posts
    end

    private

    def filter_category
      return if params[:category].blank?
      @posts = posts.where(category: Post.categories[params[:category].downcase])
    end

    def filter_tag
      return if params[:tag].blank?
      @posts = posts.where("tags @> '{#{params[:tag].downcase}}'")
    end

    def filter_author
      return if params[:author].blank?
      @posts = posts.where("admin_users.slug ilike ?", params[:author])
    end

    def filter_page
      return if params[:page].blank?
      @posts = posts.offset(params[:page].to_i * 10)
    end
  end
end
