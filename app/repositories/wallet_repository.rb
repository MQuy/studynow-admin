class WalletRepository
  extend Helpers::RepoAttr

  attr_record :wallet

  def initialize(wallet_params = {})
    @wallet = Wallet.new(wallet_params)
  end

  def receive(price, category, data = {})
    change_price(price, category, data)
  end

  def withdraw(price, category, data = {})
    change_price(-price, category, data)
  end

  private

  def change_price(price, category, data)
    if wallet.update(price: wallet.price + price)
      wallet.transactions.create(price: price, category: category, data: data)
    end
  end
end
