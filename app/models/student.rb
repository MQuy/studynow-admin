class Student < User

  has_many :lesson_schedules, class_name: LessonSchedule, foreign_key: :student_id

  has_many :courses, foreign_key: :student_id, dependent: :destroy
  has_many :course_teachers, through: :courses, source: :course_teachers
  has_many :teachers, -> { distinct }, through: :course_teachers, source: :teacher

  has_many :active_courses, -> { in_process }, class_name: Course, foreign_key: :student_id, dependent: :destroy
  has_many :active_course_teachers, -> { active  }, through: :courses, source: :course_teachers
  has_many :active_teachers, -> { distinct }, through: :active_course_teachers, source: :teacher

  has_one :latest_course, -> { order('id DESC') }, class_name: Course, foreign_key: :student_id

  has_many :homework_students, foreign_key: :student_id
  has_many :homeworks, through: :homework_students
end
