class HomeworkStudent < ActiveRecord::Base

  belongs_to :homework
  belongs_to :student, class_name: Student
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :homework_student_comments, class_name: Comment, as: :commentable, dependent: :destroy

end
