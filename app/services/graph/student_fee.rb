module Graph
  class StudentFee
    attr_reader :student_ids

    def initialize(student_ids)
      @student_ids = student_ids
    end

    def execute
      Transaction.with_students(student_ids).find_each do |transaction|
        student_id = transaction.data['student_id']
        distance = month_distance(min_time, transaction.created_at)
        fees[student_id][distance] += transaction.price.exchange_to(:vnd).cents
      end
    end

    def values
      @values ||= (execute or fees)
    end

    def labels
      @labels ||= (month_distance(min_time, Time.now) + 1).times.map { |i| (min_time + i.months).strftime("%m-%y") }
    end

    private

    def month_distance(date1, date2)
      (date2.year * 12 + date2.month) - (date1.year * 12 + date1.month)
    end

    def fees
      @fees ||= student_ids.map { |id| [id, Array.new(month_distance(min_time, Time.now) + 1, 0)] }.to_h
    end

    def min_time
      @min_time ||= [Transaction.minimum(:created_at), 11.months.ago].min
    end
  end
end
