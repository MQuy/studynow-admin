Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  post '/paper_trail_versions/:id/revert', to: 'paper_trail_versions#revert', as: :revert_paper_trail_version

  resources :charts do
    collection do
      get :student_fees
    end
  end

  resources :graphql, only: [:index, :create]

  mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"

  mount Client::Route => '/client'
  mount Partner::Route => '/agency'
end
