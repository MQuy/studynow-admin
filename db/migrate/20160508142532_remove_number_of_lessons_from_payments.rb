class RemoveNumberOfLessonsFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :number_of_lessons, :integer
  end
end
