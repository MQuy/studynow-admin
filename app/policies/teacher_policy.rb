class TeacherPolicy < UserPolicy

  def withdraw?
    user.superadmin?
  end

  class Scope < Scope

    def resolve
      if user.admin?
        scope.all
      elsif user.manager?
        scope.where(id: user.teacher_ids)
      else
        scope.none
      end
    end
  end
end

