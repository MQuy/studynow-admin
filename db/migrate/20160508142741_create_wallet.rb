class CreateWallet < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.belongs_to :user
      t.monetize :price

      t.timestamps
    end
  end
end
