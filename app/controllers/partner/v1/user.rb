module Partner
  module V1
    class User < Grape::API
      include Helper

      helpers do
        def create_params
          ActionController::Parameters.new(params)
            .require(:user).permit(:email, :nickname, :full_name, :password, :password_confirmation, :type)
        end

        def user_params
          ActionController::Parameters.new(params).require(:user).permit!
        end
      end

      resource :users do

        post :sign_in do
          user = ::Agency.find_by(email: user_params[:email]&.downcase)
          if user && user.valid_password?(user_params[:password])
            render user
          else
            error!(['Email or Password is not correct'], 401)
          end
        end

        get :me do
          if current_user
            render current_user
          else
            error!('401 Unauthorized', 401)
          end
        end

        get :referral_users do
          if current_user
            render current_user.referral_users.with_transactions(current_user.id), each_serializer: ReferralUserSerializer
          else
            error!('401 Unauthorized', 401)
          end
        end

      end
    end
  end
end
