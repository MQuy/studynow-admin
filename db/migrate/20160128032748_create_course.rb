class CreateCourse < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.belongs_to :teacher
      t.belongs_to :student
      t.string :code
      t.boolean :trial, default: false
      t.integer :number_of_lessons
      t.date :started_date

      t.timestamps
    end
  end
end
