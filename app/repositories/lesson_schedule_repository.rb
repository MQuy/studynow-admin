class LessonScheduleRepository
  extend Helpers::RepoAttr

  attr_record :lesson_schedule

  class << self
    def create(params)
      course = Course.find(params[:course_id])
      time_zone = ActiveSupport::TimeZone[course.time_zone]
      params[:started_time] ||= "#{params[:started_date]} #{params[:started_hour]}"
      started_time = time_zone.parse(params[:started_time])
      lesson_duration = params.delete(:length).to_i
      ended_time = started_time + (lesson_duration.zero? ? course.lesson_duration : lesson_duration).minutes

      lesson_schedule_params = params.except(:started_date, :started_hour).merge(
        teacher_id: course.active_teacher.id,
        student_id: course.student_id,
        status: params[:status] || :official,
        started_time: started_time,
        ended_time: ended_time
      )
      repo = new(lesson_schedule_params)
      repo.save
      repo
    end
  end

  def initialize(lesson_schedule_params = {})
    @lesson_schedule = LessonSchedule.new(lesson_schedule_params)
  end

  def cancel!
    lesson_schedule.cancel!
  rescue ActiveRecord::RecordInvalid
    false
  end
end
