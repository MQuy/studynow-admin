class RemoveCodeFromAdminUser < ActiveRecord::Migration
  def change
    remove_column :admin_users, :code, :string
  end
end
