module MessageQueue
  extend self

  def configure
    yield(config) if block_given?
  end

  def config
    @config ||= Configuration.new
  end

  def worker(name, payload)
    config.strategy.worker(name, payload)
  end
end
