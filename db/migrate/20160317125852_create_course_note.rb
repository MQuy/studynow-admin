class CreateCourseNote < ActiveRecord::Migration
  def change
    create_table :course_notes do |t|
      t.belongs_to :course
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
