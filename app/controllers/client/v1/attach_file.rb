module Client
  module V1
    class AttachFile < Grape::API
      include Helper

      helpers do
        def attach_file_params
          ActionController::Parameters.new(params).require(:attach_file).permit!
        end
      end

      resource :attach_files do
      end
    end
  end
end
