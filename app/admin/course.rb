ActiveAdmin.register Course do
  actions :all
  permit_params :creator_id, :student_id, :price_units, :price_currency, :number_of_lessons, :number_of_reschedule, :lesson_duration, :started_date, :trial, :time_zone, :_destroy,
    course_schedules_attributes: [:id, :block_range_day_begin, :block_range_time_begin, :block_range_day_end, :block_range_time_end, :block_range_length],
    course_teachers_attributes: [:id, :teacher_id, :status]

  filter :course_teachers, as: :select, collection: Teacher.active.order(:full_name).pluck(:full_name, :id)
  filter :student, as: :select, collection: Student.active.order(:full_name).pluck(:nickname, :id)
  filter :creator
  filter :code
  filter :number_of_reschedule

  scope(:all)
  scope(:in_process, default: true)
  scope(:need_to_swapped) { |scope| scope.will_be_swapped }

  action_item :teaching_schedules, only: :show do
    link_to('Schedule', course_lesson_schedules_path(course_id: course.id))
  end

  action_item :course_notes, only: :show do
    link_to('Notes', course_course_notes_path(course_id: course.id))
  end

  action_item :course_teachers, only: :show do
    link_to('Teachers', course_course_teachers_path(course_id: course.id))
  end

  action_item :course_comments, only: :show do
    link_to('Comments', course_course_comments_path(course_id: course.id))
  end

  action_item :course_state, only: :show do
    if course.in_process?
      links = ''
      if authorized? :complete, course
        links += link_to('Complete', complete_course_path(course), method: :post)
      end
      if authorized? :cancel, course
        links += link_to('Cancel', cancel_course_path(course), method: :post, class: 'mg-left-6px')
      end
      links.html_safe
    end
  end

  sidebar :versionate, partial: "admin/version", only: :show, if: -> { current_admin_user.superadmin? }

  index do
    id_column
    column('Teacher') { |course| link_to course.active_teacher.full_name, user_path(course.active_teacher) }
    column('Student') { |course| link_to course.student.nickname, user_path(course.student) }
    column :number_of_lessons
    column :number_of_reschedule
    column('Learned Duration (h)') { |course| course.learned_lessons.to_a.sum { |ls| (ls.length.to_f / 60).round(2) } }
    column :started_date
    actions
  end

  show do
    attributes_table do
      row :id
      row :code
      row('Status') do |course|
        status_tag(course.status, class: admin_statuses[course.status])
      end
      row :student
      row('Price') { |course| money_format(course.price) }
      row :number_of_lessons
      row :number_of_reschedule
      row :lesson_duration
      row('Learned Duration (h)') { |course| course.learned_lessons.to_a.sum { |ls| (ls.length.to_f / 60).round(2) } }
      row :started_date
      row :created_at
      row :time_zone
      row :trial
    end
    panel 'Teachers' do
      attributes_table_for resource do
        row("Active Teacher") { |course| link_to course.active_teacher.full_name, user_path(course.active_teacher) }
        course.course_teachers.where.not(status: :active).order(:assigned_at).find_each.with_index do |course_teacher, index|
          row("Expired Teacher #{index + 1}") { link_to course_teacher.teacher.full_name, user_path(course_teacher.teacher) }
        end
      end
    end
    panel 'Weekly Schedule' do
      attributes_table_for resource do
        resource.course_schedules.each_with_index do |cs, index|
          row("Weekday #{index + 1}") { block_range_format(cs, course.time_zone) }
        end
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    if f.object.persisted?
      render 'admin/courses/edit', context: self, f: f
    else
      render 'admin/courses/new', context: self, f: f
    end
    actions
  end

  controller do
    def new
      @course = Course.new
      @course.course_teachers.build(status: :active)
    end

    def create
      params[:course][:creator_id] = current_admin_user.id
      repo = CourseRepository.create(permitted_params[:course])
      @course = repo.course

      if repo.persisted?
        redirect_to course_path(repo.id), notice: "Course is created successfully."
      else
        render action: :new
      end
    end

    def destroy
      repo = CourseRepository.load(resource)

      if repo.destroy
        redirect_to courses_path, notice: "Course is deleted successfully"
      else
        redirect_to courses_path, notice: "There is problem when deleting this course"
      end
    end
  end

  member_action :complete, method: :post do
    if CourseRepository.load(resource).complete
      redirect_to courses_path, notice: "Course is completed successfully."
    else
      render action: :new
    end
  end

  member_action :cancel, method: :post do
    if CourseRepository.load(resource).cancel
      redirect_to courses_path, notice: "Course is canceled successfully."
    else
      render action: :new
    end
  end
end
