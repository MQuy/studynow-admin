class CreateNotification < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :notificable, polymorphic: true
      t.belongs_to :user
      t.belongs_to :creator
      t.integer :action
      t.boolean :read, default: false
      t.json :data

      t.timestamps
    end
  end
end
