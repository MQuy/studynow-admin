module MessageQueue
  class Configuration
    attr_reader :strategy

    def strategy=(name)
      @strategy = Strategies.const_get(name.to_s.titleize).new
    end
  end
end
