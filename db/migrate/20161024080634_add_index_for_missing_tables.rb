class AddIndexForMissingTables < ActiveRecord::Migration
  def change
    add_index :attach_files, :user_id
    add_index :attach_files, [:attachable_id, :attachable_type]

    add_index :comments, :user_id
    add_index :comments, [:commentable_id, :commentable_type]

    add_index :course_notes, :course_id

    add_index :course_schedules, :course_id

    add_index :course_teachers, :course_id
    add_index :course_teachers, :teacher_id

    add_index :courses, :student_id

    add_index :lesson_schedules, :course_id
    add_index :lesson_schedules, :teacher_id
    add_index :lesson_schedules, :student_id

    add_index :notifications, :user_id
    add_index :notifications, [:notificable_id, :notificable_type]

    add_index :payments, :user_id

    add_index :posts, :author_id

    add_index :teacher_profiles, :user_id

    add_index :transactions, [:payable_id, :payable_type]

    add_index :user_providers, :user_id
    add_index :user_providers, :provider

    add_index :wallets, :user_id

    add_index :weekly_schedules, :teacher_id
  end
end
