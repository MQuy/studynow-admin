QueryType = GraphQL::ObjectType.define do
  name "Query"
  description "The query root of this schema. See available queries."

  # Get Post by ID
  field :post, PostType do
    argument :id, types.ID
    argument :slug, types.String

    resolve -> (obj, args, ctx) { Post.friendly.find(args[:id] || args[:slug]) }
  end

  field :posts do
    type GraphQL::ListType.new(of_type: PostType)
    argument :category, types.String, "category of posts"
    argument :tag, types.String, "The tag of posts"
    argument :author, types.String, "Author's name of posts"
    argument :page, types.Int, "Paginate posts"

    resolve PostsResolve
  end

  field :notifications do
    type GraphQL::ListType.new(of_type: NotificationType)

    argument :user_id, types.Int, "The user of notifications"
    argument :page, types.Int, "Paginrate notifications"

    resolve NotificationsResolve
  end

  field :unread_notifications_count do
    type types.Int

    argument :user_id, types.Int, "The user of notifications"

    resolve -> (obj, args, ctx) { Notification.unread.where(user_id: args[:user_id]).count }
  end
end
