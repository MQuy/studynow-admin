module Client
  module V1
    class Notification < Grape::API
      include Helper

      resource :notifications do
        post 'mark_as_read' do
          if params[:user_id]
            current_user.notifications.update_all(read: true)
          elsif params[:id]
            ::Notification.find(params[:id]).update(read: true)
          end
          render({ }, serializer: EmptySerializer)
        end
      end
    end
  end
end
