module MailerQueue
  module Helper

    def worker(email, payload)
      queue_name, mailer_name = parse_information
      method_name = caller_locations(1,1)[0].label

      MessageQueue.worker(queue_name, {
        name: mailer_name,
        action: method_name,
        args: [email, payload]
      })
    end

    def parse_information
      name
        .split("::")
        .map { |part| part.gsub("Worker", "") }
        .map &:underscore
    end
  end
end
