class UpcomingSchedulesFilterService
  attr_accessor :current_user, :params

  def initialize(user, params)
    @current_user, @params = user, params
  end

  def execute
    upcoming_schedules
  end

  def upcoming_schedules
    @upcoming_schedules ||= UserRepository.load(current_user).upcoming_schedules
  end
end
