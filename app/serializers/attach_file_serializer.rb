class AttachFileSerializer < ActiveModel::Serializer
  attributes :id, :path, :content_type, :file_size, :file_name

  def path
    object.path.url
  end
end
