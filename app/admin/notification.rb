ActiveAdmin.register Notification do
  actions :all, except: [:new, :destroy]
  menu false

  index do
    id_column
    column('Notificable') do |notification|
      notificable_url = send("#{notification.notificable_type.underscore}_path", notification.notificable_id)
      link_to("#{notification.notificable_type} - #{notification.notificable_id}", notificable_url)
    end
    column :creator
    column :user
    column :action
    column :read
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row('Notificable') do |notification|
        notificable_url = send("#{notification.notificable_type.underscore}_path", notification.notificable_id)
        link_to("#{notification.notificable_type} - #{notification.notificable_id}", notificable_url)
      end
      row :creator
      row :user
      row :action
      row :read
      row :data
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
