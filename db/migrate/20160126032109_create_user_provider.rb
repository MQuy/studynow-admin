class CreateUserProvider < ActiveRecord::Migration
  def change
    create_table :user_providers do |t|
      t.belongs_to :user
      t.integer :provider
      t.string :uid
      t.string :token
      t.string :secret

      t.timestamps
    end
  end
end
