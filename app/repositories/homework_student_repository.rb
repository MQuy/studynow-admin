class HomeworkStudentRepository
  extend Helpers::RepoAttr

  attr_record :homework_student

  def initialize(homework_student_params = {})
    @homework_student = HomeworkStudent.new(homework_student_params)
  end
end
