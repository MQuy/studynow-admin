module Range::Int4range
  class Decorator
    attr_accessor :object

    def initialize(object)
      @object = object
    end

    def boundary
      "#{Date::DAYNAMES[object.boundary / 48 % 7]}, #{pad(object.boundary % 48 / 2)}:#{pad((object.boundary % 2) * 30)}"
    end

    def fragment
      object.type == :day ? object.boundary / 48 % 7 : "#{pad((object.boundary % 48) / 2)}:#{pad((object.boundary % 2) * 30)}"
    end

    private

    def pad(v, r = 2)
      "%0#{r}d" % v
    end
  end
end
