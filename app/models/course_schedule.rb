class CourseSchedule < ActiveRecord::Base
  extend Range::Base

  range_assign :block_range

  belongs_to :course

  def shift_on_distance(distance)
    return unless self.block_range_begin || self.block_range_end

    self.block_range_begin = self.block_range_begin - distance
    self.block_range_end = self.block_range_end - distance

    if self.block_range_begin < 0
      self.block_range_begin += 336
    end

    if self.block_range_end < 0
      self.block_range_end += 336
    end

    if self.block_range_begin > self.block_range_end
      self.block_range_end += 336
    end
  end
end
