ActiveAdmin.register HomeworkStudent do
  belongs_to :homework
  permit_params :score

  action_item :comments, only: :show do
    link_to('Comments', homework_student_homework_student_comments_path(homework_student_id: homework_student.id))
  end

  filter :student
  filter :score

  index do
    selectable_column
    id_column
    column :student
    column('Number of comments') do |homework_student|
      link_to homework_student.comments.count, homework_student_homework_student_comments_path(homework_student_id: homework_student.id)
    end
    column('Latest comment') do |homework_student|
      homework_student.comments.order(id: :desc).first.content rescue ''
    end
    column('Latest Attachment') do |homework_student|
      if attach_file = homework_student.comments.order(id: :desc).first&.attach_file
        link_to 'Download', attach_file.path.url
      end
    end
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Homework Student' do
      f.input :score
    end
    f.actions
  end
end
