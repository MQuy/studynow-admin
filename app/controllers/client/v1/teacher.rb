module Client
  module V1
    class Teacher < Grape::API
      include Helper

      helpers do
        def repository
          TeacherRepository.find(params[:id])
        end

        def teacher_params
          ActionController::Parameters.new(params)
            .require(:teacher).permit(weekly_schedules_attributes: [:id, :teacher_id, :_destroy, :block_range_begin, :block_range_end])
        end
      end

      resource :teachers do
        get ':id' do
          respond_with(repository)
        end

        get ':id/students' do
          render repository.teacher.active_students, each_serializer: StudentSerializer
        end

        put ':id/weekly_schedules' do
          if repository.update_weekly_schedules(teacher_params[:weekly_schedules_attributes])
            render(repository.teacher.weekly_schedules, each_serializer: WeeklyScheduleSerializer)
          else
            render({ errors: ['Something went wrong.'] }, serializer: EmptySerializer)
          end
        end
      end
    end
  end
end
