class FeedbackRepository
  extend Helpers::RepoAttr

  attr_record :feedback

  def initialize(feedback_params)
    @feedback = Feedback.new(feedback_params)
  end
end
