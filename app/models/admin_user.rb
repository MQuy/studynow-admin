class AdminUser < ActiveRecord::Base
  extend FriendlyId

  enum role: [:superadmin, :admin, :manager, :editor]

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :creating_users, class_name: User, foreign_key: :creator_id

  friendly_id :slug_candidates, use: [:slugged, :finders]

  def admin?
    super || superadmin?
  end

  def manager?
    super || superadmin?
  end

  def editor?
    super || admin?
  end

  private

  def slug_candidates
    [
      :full_name,
      [:id, :full_name],
      [:email]
    ]
  end
end
