class CreateLessonPackage < ActiveRecord::Migration
  def change
    create_table :lesson_packages do |t|
      t.belongs_to :user
      t.integer :category
      t.integer :number_of_lessons
      t.integer :remain_of_lessons

      t.timestamps
    end
  end
end
