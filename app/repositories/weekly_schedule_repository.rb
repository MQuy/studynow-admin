class WeeklyScheduleRepository
  extend Helpers::RepoAttr

  attr_record :weekly_schedule
  attr_accessor :time_zone

  def initialize(weekly_schedule_params = {}, time_zone = nil)
    @weekly_schedule = WeeklySchedule.new(weekly_schedule_params)
    @time_zone = time_zone || weekly_schedule.teacher.time_zone

    convert_into_zero_zone
  end

  def convert_into_zero_zone
    distance = Time.now.in_time_zone(time_zone).utc_offset / 3600 * 2
    weekly_schedule.shift_on_distance(distance)
  end
end
