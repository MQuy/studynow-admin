class User < ActiveRecord::Base
  extend FriendlyId
  include UniqueCode
  include CarrierWaveDirectConcern

  enum status: [:pending, :active, :ban]

  friendly_id :slug_candidates, use: [:slugged, :finders]
  mount_uploader :avatar, ImageUploader
  has_paper_trail on: [:create, :update, :destroy]

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :lockable, :omniauthable, :token_authenticatable, :lockable

  belongs_to :referrer, class_name: User, foreign_key: :referral_code, primary_key: :code
  belongs_to :creator, class_name: AdminUser, foreign_key: :creator_id
  has_many :user_providers, dependent: :destroy
  has_many :referral_users, class_name: User, foreign_key: :referral_code, primary_key: :code
  has_one :wallet, dependent: :destroy
  has_many :transactions, through: :wallet
  has_many :payments, dependent: :destroy
  has_many :attach_files, dependent: :destroy
  has_many :notifications, dependent: :destroy

  before_create :ensure_authentication_token, :ensure_nickname, :ensure_currency

  scope :students, -> { where(type: 'Student') }
  scope :teachers, -> { where(type: 'Teacher') }
  scope :students_or_teachers, -> { where(type: ['Student', 'Teacher']) }
  scope :agencies, -> { where(type: 'Agency') }
  scope :with_transactions, -> (referrer_id) {
    join_sql = <<-SQL
      LEFT JOIN (
        SELECT (transactions.data ->> 'student_id')::int as student_id, sum(transactions.price_units)
        FROM transactions
        INNER JOIN wallets
        ON wallets.id = transactions.payable_id AND transactions.payable_type = 'Wallet'
        WHERE wallets.user_id = #{referrer_id} AND transactions.data ->> 'student_id' IS NOT NULL
        GROUP BY transactions.data ->> 'student_id'
      ) t ON users.id = t.student_id
    SQL
    joins(join_sql)
      .select('users.*, t.sum as referral_price')
  }

  [:teacher, :student, :agency].each do |t|
    define_method "#{t}?" do
      type.underscore == t.to_s
    end
  end

  def slug_candidates
    [
      :full_name,
      [:id, :full_name],
      [:email]
    ]
  end

  def ensure_nickname
    self.nickname ||= full_name
  end

  def ensure_currency
    self.currency ||= 'VND'
  end
end
