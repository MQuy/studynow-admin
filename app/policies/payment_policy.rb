class PaymentPolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def pay?
    user.admin?
  end

  def update?
    super && !record.approved?
  end
end
