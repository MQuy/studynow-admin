class FeedbackPolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def in_process?
    user.admin?
  end

  def complete?
    user.admin?
  end
end
