class CourseArraySerializer < ActiveModel::Serializer
  attributes :id, :remain_of_lessons, :number_of_lessons, :started_date, :code
  attributes :teacher, :student, :price, :number_of_hours

  def teacher
    UserArraySerializer.new(object.active_teacher)
  end

  def student
    UserArraySerializer.new(object.student)
  end

  def price
    object.price.format
  end

  def number_of_hours
    object.lesson_schedules.be_learned.all.map(&:length).inject(:+).to_f / 60
  end
end
