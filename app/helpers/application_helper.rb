module ApplicationHelper
  def calendar_days_opts
    Date::DAYNAMES.map.with_index { |d, i| [d, i] }
  end

  def block_range_format(object, time_zone)
    distance = Time.now.in_time_zone(time_zone).utc_offset / 3600 * 2
    object.shift_on_distance(-distance)
    begin_format = Range::Int4range::Attributes.new(object, :block_range, :nil, :begin).decorator.boundary
    end_format = Range::Int4range::Attributes.new(object, :block_range, :nil, :end).decorator.boundary
    "#{time_zone} - #{begin_format} -> #{end_format}"
  end

  def user_type_opts
    ['Student', 'Teacher'].map { |r| [r, r] }
  end

  def admin_statuses
    { 'insert' => 'ok', 'cancel' => 'warning', 'student' => 'no', 'teacher' => 'yes', 'approved' => 'ok', 'superadmin' => 'red', 'admin' => 'warning', 'manager' => 'ok', 'today' => 'red', 'yesterday' => 'green', 'tomorrow' => 'yes', 'in_process' => 'ok', 'completed' => 'red', 'active' => 'red', 'success' => 'ok', 'failure' => 'red' }
  end

  def lesson_schedule_opts
    LessonSchedule.statuses.map { |k, v| [k.titleize, k] }
  end

  def payment_category_opts
    Payment.categories.map { |k, v| [k.titleize, k] }
  end

  def admin_role_opts
    AdminUser.roles.map { |k, v| [k.titleize, k] }
  end

  def free_lesson_opts
    FreeLessonRegister.statuses.map { |k, v| [k.titleize, k] }
  end

  def course_teacher_opts
    CourseTeacher.statuses.map { |k, v| [k.titleize, k] }
  end

  def money_format(price)
    if Payment::CURRENIES.include? price.currency_as_string
      number_to_currency(price.cents, unit: price.currency_as_string, precision: 0, format: '%n %u')
    else
      price.format
    end
  end

  def user_statuses
    { 'update' => 'ok', 'destroy' => 'error', 'create' => 'yes' }
  end
end
