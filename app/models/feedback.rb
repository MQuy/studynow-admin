class Feedback < ActiveRecord::Base
  enum status: [:pending, :in_process, :completed]
end
