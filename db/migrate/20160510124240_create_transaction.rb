class CreateTransaction < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :payable, polymorphic: true
      t.monetize :price
      t.integer :category, null: false
      t.string :reason
      t.json :data

      t.timestamps
    end
  end
end
