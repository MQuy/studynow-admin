module NotificationService
  module Comment
    extend self

    def create(comment)
      case comment.commentable_type
      when 'Course'
        from_course(comment, comment.commentable)
      end
    end

    def from_course(comment, course)
      user_id = if comment.user_id == course.student_id
        course.active_teacher.id
      else
        course.student_id
      end
      comment.notifications.create(
        user_id: user_id,
        creator_id: comment.user_id,
        action: :created,
        data: { course_code: course.code }
      )
    end
  end
end
