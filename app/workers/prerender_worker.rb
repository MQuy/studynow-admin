class PrerenderWorker
  include Sidekiq::Worker

  def perform(url)
    Typhoeus.post(
      "http://api.prerender.io/recache",
      body: {
        prerenderToken: "hyxb9SVFL4Qg1wPPgFuu",
        url: url
      }
    )
  end
end

