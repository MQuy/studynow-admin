class AddTmpUrlToAttachFile < ActiveRecord::Migration
  def change
    add_column :attach_files, :remote_path_tmp_url, :string
  end
end
