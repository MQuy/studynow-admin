module MessageQueue
  module Strategies
    class Rabbitmq

      def initialize
        @conn = Bunny.new
        @conn.start
        @channel = conn.create_channel
      end

      def worker(name, payload)
        queue = channel.queue(name, durable: true)
        exchange = channel.default_exchange
        exchange.publish(JSON.dump(payload), routing_key: queue.name)
      end

      private

      attr_reader :conn, :channel, :queue, :exchange
    end
  end
end
