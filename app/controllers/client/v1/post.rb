module Client
  module V1
    class Post < Grape::API
      include Helper

      resource :posts do
        get do
          posts = Search::PostService.new(params).execute
          render(posts, each_serializer: PostSerializer)
        end

        get ':slug' do
          post = ::Post.friendly.find(params[:slug])
          render post
        end
      end
    end
  end
end
