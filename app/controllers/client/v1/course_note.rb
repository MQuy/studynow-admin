module Client
  module V1
    class CourseNote < Grape::API
      include Helper

      helpers do
        def course_note_params
          ActionController::Parameters.new(params)
            .require(:course_note).permit(:course_id, :title, :description)
        end
      end

      resource :course_notes do
        post do
          course_note = CourseNoteRepository.create(course_note_params)
          respond_with(course_note)
        end

        delete ':id' do
          course_note = CourseNoteRepository.find(params[:id])
          respond_with(course_note, :destroy)
        end
      end
    end
  end
end
