class CreateAttachFile < ActiveRecord::Migration
  def change
    create_table :attach_files do |t|
      t.references :attachable, polymorphic: true
      t.belongs_to :user
      t.string :path
      t.string :content_type
      t.integer :file_size
      t.string :state

      t.timestamps
    end
  end
end
