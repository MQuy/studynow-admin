class AddRoleToAdminUser < ActiveRecord::Migration
  def migrate(direction)
    super

    AdminUser.update_all(role: AdminUser.roles[:superadmin]) if direction == :up
  end

  def change
    add_column :admin_users, :role, :integer
  end
end
