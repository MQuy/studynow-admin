module Helpers
  module ManagerPolicy
    def index?
      user.manager?
    end

    def show?
      user.manager?
    end

    def update?
      user.manager?
    end

    def create?
      user.manager?
    end

    def destroy?
      user.manager?
    end
  end
end
