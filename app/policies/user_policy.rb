class UserPolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def active?
    user.admin?
  end

  def destroy?
    user.superadmin?
  end
end
