UserType = GraphQL::ObjectType.define do
  name "User"
  description "An admin user entry, return basic user information"

  field :id, types.ID
  field :full_name, types.String
  field :email, types.String
  field :slug, types.String
  field :phone_number, types.String
  field :skype, types.String
  field :address, types.String
  field :avatar, types.String do
    resolve -> (obj, args, ctx) { obj.remote_avatar_tmp_url || obj.avatar }
  end
  field :authentication_token, types.String do
    resolve -> (obj, args, ctx) do
      ctx[:current_user].id == obj.id ? obj.authentication_token : nil
    end
  end
  field :type, types.String do
    resolve -> (obj, args, ctx) { obj.type }
  end
  field :code, types.String
  field :creator, AdminUserType
  field :referral_code, types.String
  field :currency, types.String
  field :time_zone, types.String
end
