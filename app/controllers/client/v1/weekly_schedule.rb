module Client
  module V1
    class WeeklySchedule < Grape::API
      include Helper

      helpers do
        def weekly_schedule_params
          ActionController::Parameters.new(params)
            .require(:weekly_schedule).permit(
              :teacher_id, :block_range_day_begin, :block_range_time_begin,
              :block_range_day_end, :block_range_time_end
            )
        end
      end

      resource :weekly_schedules do
        post do
          repository = WeeklyScheduleRepository.create(weekly_schedule_params)
          respond_with(repository)
        end

        delete ':id' do
          repository = WeeklyScheduleRepository.find(params[:id])
          respond_with(repository)
        end
      end
    end
  end
end
