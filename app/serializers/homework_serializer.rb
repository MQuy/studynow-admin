class HomeworkSerializer < ActiveModel::Serializer
  attributes :id, :content, :code, :attach_file, :admin_user

  def attach_file
    AttachFileSerializer.new(object.attach_file)
  end

  def admin_user
    AdminUserSerializer.new(object.admin_user)
  end
end
