class CommentRepository
  extend Helpers::RepoAttr

  attr_record :comment

  class << self
    def create(args)
      repo = new(args)
      comment = repo.comment

      ActiveRecord::Base.transaction do
        if comment.save!
          NotificationService::Comment.create(comment)
        end
      end
    ensure
      return repo
    end
  end

  def initialize(comment_params = {})
    @comment = Comment.new(comment_params)
  end
end
