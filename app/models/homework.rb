class Homework < ActiveRecord::Base
  include UniqueCode

  belongs_to :admin_user
  has_one :attach_file, as: :attachable, dependent: :destroy
  has_many :homework_students, dependent: :destroy

  accepts_nested_attributes_for :attach_file, allow_destroy: true
end
