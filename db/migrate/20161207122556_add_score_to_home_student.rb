class AddScoreToHomeStudent < ActiveRecord::Migration
  def change
    add_column :homework_students, :score, :decimal, precision: 5, scale: 2, default: 0
  end
end
