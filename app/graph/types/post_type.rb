PostType = GraphQL::ObjectType.define do
  name "Post"
  description "A single post entry returns a post with author, content"

  field :id, types.ID
  field :title, types.String
  field :content, types.String
  field :markdown, types.String
  field :category, types.String do
    resolve -> (obj, args, ctx) { obj.category }
  end
  field :slug, types.String
  field :cover_image, types.String
  field :brief, types.String
  field :status, types.String do
    resolve -> (obj, args, ctx) { obj.status }
  end
  field :author, AdminUserType
  field :tags, types[types.String]
end
