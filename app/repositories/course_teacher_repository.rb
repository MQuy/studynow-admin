class CourseTeacherRepository
  extend Helpers::RepoAttr

  attr_record :course_teacher

  class << self
    def create(params)
      course = Course.find(params[:course_id])
      time_zone = ActiveSupport::TimeZone[course.time_zone]
      assigned_at = Time.zone.parse(params[:assigned_at])
      expired_at = Time.zone.parse(params[:expired_at])
      status = params[:status]

      course_teacher_params = params.merge(
        assigned_at: assigned_at,
        expired_at: expired_at,
        status: status
      )

      repo = new(course_teacher_params)
      ActiveRecord::Base.transaction do
        if repo.save!
          repo.deactive_other_teachers
          repo.change_lesson_schedules
        end
      end
    ensure
      return repo
    end
  end

  def initialize(course_teacher_params = {})
    @course_teacher = CourseTeacher.new(course_teacher_params)
  end

  def deactive_other_teachers
    return unless course_teacher.active?

    CourseTeacher
      .where(course_id: course_teacher.course_id)
      .where(status: 'active')
      .where.not(id: course_teacher.id)
      .update_all(status: CourseTeacher.statuses[:expired], expired_at: Time.current)
  end

  def change_lesson_schedules
    LessonSchedule
      .where(course_id: course_teacher.course_id)
      .where("started_time >= ?", course_teacher.assigned_at)
      .where("ended_time <= ?", course_teacher.expired_at || 100.years.from_now)
      .update_all(teacher_id: course_teacher.teacher_id)
  end
end

