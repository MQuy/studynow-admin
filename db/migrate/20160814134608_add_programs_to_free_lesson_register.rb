class AddProgramsToFreeLessonRegister < ActiveRecord::Migration
  def change
    add_column :free_lesson_registers, :programs, :text, array: true, default: []
  end
end
