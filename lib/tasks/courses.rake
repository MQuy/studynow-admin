namespace :courses do

  desc 'Complete courses'
  task complete: :environment do
    Course.in_process.find_each do |course|
      next if course.remain_of_lessons > 0
      CourseRepository.load(course).complete
    end
  end

  desc 'Remind courses'
  task remind_for_swap: :environment do
    Course.joins(:active_course_teacher)
          .will_be_swapped(19)
          .where("course_teachers.data ->> 'remind_for_swap' IS NULL OR course_teachers.data ->> 'remind_for_swap' = 'false'")
          .find_each do |course|
      course_teacher = course.active_course_teacher
      course_teacher.data['remind_for_swap'] = true
      course_teacher.update
      CourseMailer.delay.estimation(course.id)
    end
  end
end
