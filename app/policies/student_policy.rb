class StudentPolicy < UserPolicy

  class Scope < Scope

    def resolve
      if user.admin?
        scope.all
      else
        student_ids = Course.joins(:active_course_teacher)
                            .where(course_teachers: { teacher_id: user.teacher_ids })
                            .pluck(:student_id)
        scope.where(id: student_ids)
      end
    end
  end
end

