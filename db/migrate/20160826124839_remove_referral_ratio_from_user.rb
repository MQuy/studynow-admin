class RemoveReferralRatioFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :referral_ratio, :decimal, precision: 4, scale: 2
  end
end
