namespace :lesson_schedules do

  desc 'Lesson schedules'
  task receive_referral: :environment do
    time = 12.hour.ago
    begin_time = time.beginning_of_day
    end_time = time.end_of_day
    LessonSchedule.where("ended_time >= ? AND ended_time <= ?", begin_time, end_time).find_each do |ls|
      referrer = ls.student.referrer
      next unless referrer&.agency?

      referral_price = ls.price * AgencyRepository.load(referrer).referral_ratio(ls.student_id, ls.course_id)
      WalletRepository.load(referrer.wallet).receive(referral_price, :course_referrer, { course_id: ls.course_id, lesson_schedule_id: ls.id })
    end
  end
end
