class CreateTeacherProfile < ActiveRecord::Migration
  def change
    create_table :teacher_profiles do |t|
      t.belongs_to :user
      t.monetize :price

      t.timestamps
    end
  end
end
