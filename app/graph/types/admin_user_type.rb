AdminUserType = GraphQL::ObjectType.define do
  name "AdminUser"
  description "An admin user entry, return basic user information"

  field :id, types.ID
  field :full_name, types.String
  field :email, types.String
  field :slug, types.String
end
