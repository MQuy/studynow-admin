module Client
  module V1
    class FreeLessonRegister < Grape::API
      include Helper

      helpers do
        def free_lesson_register_params
          ActionController::Parameters.new(params)
            .require(:free_lesson_register).permit(:full_name, :email, :phone_number, :skype, programs: [])
        end
      end

      resource :free_lesson_registers do
        post do
          repository = FreeLessonRegisterRepository.create(free_lesson_register_params)
          respond_with(repository)
        end
      end
    end
  end
end
