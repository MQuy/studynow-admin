module CarrierWaveDirectConcern
  extend ActiveSupport::Concern

  included do
    after_commit :remote_image_worker, if: proc { previous_changes.include?("remote_#{self.class.uploaders.keys[0]}_tmp_url") }
  end

  def remote_image_worker
    kclass = self.class
    column_name = kclass.uploaders.keys[0]
    remote_url = send("remote_#{column_name}_tmp_url")
    return if new_record? || remote_url.blank?
    RemoteUploaderWorker.perform_async(kclass.name, id, column_name, remote_url)
  end
end
