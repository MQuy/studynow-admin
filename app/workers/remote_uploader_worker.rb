class RemoteUploaderWorker
  include Sidekiq::Worker

  def perform(class_name, object_id, column_name, remote_url)
    klass = class_name.constantize
    target = klass.find(object_id)
    relative_path = URI.parse(URI.escape(remote_url)).path[1..-1] rescue ''
    s3_url = S3Bucket.object(URI.unescape(relative_path)).presigned_url(:get)
    target.send("remote_#{column_name}_url=", s3_url)
    target.send("remote_#{column_name}_tmp_url=", nil)
    target.save!
  end

  private

  def remove_tmp_url(relative_path)
    S3Bucket.objects.delete relative_path
  end
end
