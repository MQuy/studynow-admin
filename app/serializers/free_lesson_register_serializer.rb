class FreeLessonRegisterSerializer < ActiveModel::Serializer
  attributes :full_name, :email, :phone_number, :skype
end
