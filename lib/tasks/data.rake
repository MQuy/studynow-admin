namespace :data do
  task time_zone: :environment do
    User.update_all(time_zone: 'Hanoi')
    WeeklySchedule.find_each do |weekly_schedule|
      if weekly_schedule.teacher.blank?
        weekly_schedule.destroy
      else
        distance = Time.now.in_time_zone(weekly_schedule.teacher.time_zone).utc_offset / 3600 * 2
        weekly_schedule.shift_on_distance(distance)
        weekly_schedule.save!
      end
    end
    Teacher.update_all(time_zone: 'Kuala Lumpur')
    CourseSchedule.find_each do |course_schedule|
      distance = Time.now.in_time_zone(course_schedule.course.time_zone).utc_offset / 3600 * 2
      course_schedule.shift_on_distance(distance)
      course_schedule.save!
    end
  end
end
