class HomeworkStudentSerializer < ActiveModel::Serializer
  belongs_to :homework

  attributes :id, :score, :created_at
end
