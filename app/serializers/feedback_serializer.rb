class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :email, :phone_number, :content
end
