class Teacher < User

  has_many :weekly_schedules, foreign_key: :teacher_id, dependent: :destroy
  has_many :lesson_schedules, class_name: LessonSchedule, foreign_key: :teacher_id

  has_many :course_teachers, foreign_key: :teacher_id
  has_many :courses, through: :course_teachers, source: :course
  has_many :students, -> { distinct }, through: :courses, source: :student

  has_many :active_course_teachers, -> { active }, class_name: CourseTeacher, foreign_key: :teacher_id
  has_many :active_courses, -> { in_process }, through: :active_course_teachers, source: :course
  has_many :active_students, -> { distinct }, through: :active_courses, source: :student

  has_one :teacher_profile, foreign_key: :user_id

  accepts_nested_attributes_for :weekly_schedules, allow_destroy: true
  accepts_nested_attributes_for :teacher_profile, allow_destroy: true

  def manager
    AdminUser.find_by("teacher_ids @> '{?}'", id)
  end
end
