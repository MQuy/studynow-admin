class AddCategoryToComment < ActiveRecord::Migration
  def change
    add_column :comments, :category, :string, default: "normal"
  end
end
