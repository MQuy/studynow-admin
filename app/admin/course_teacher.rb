ActiveAdmin.register CourseTeacher do
  belongs_to :course
  permit_params :id, :course_id, :teacher_id, :assigned_at, :expired_at, :status

  index do
    id_column
    column :course
    column :teacher
    column('Status') do |ct|
      status_tag(ct.status, class: admin_statuses[ct.status])
    end
    column :assigned_at do |ct|
      ct.assigned_at.in_time_zone(ct.course.time_zone).strftime("%B %d, %Y %H:%M %z")
    end
    column :expired_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :course
      row :teacher
      row :status do |ct|
        status_tag(ct.status, class: admin_statuses[ct.status])
      end
      row :assigned_at do |ct|
        ct.assigned_at.in_time_zone(ct.course.time_zone).strftime("%B %d, %Y %H:%M %z")
      end
      row :expired_at
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    inputs 'Course Teacher' do
      f.input :course_id, input_html: { hidden: true }
      f.input :teacher, label: 'Teacher', collection: Pundit.policy_scope(current_admin_user, Teacher)
      f.input :status, as: :select, collection: course_teacher_opts, include_blank: false
      f.input :assigned_at, as: :string, input_html: { class: 'datetime-picker' }
      f.input :expired_at, as: :string, input_html: { class: 'datetime-picker' }
    end
    f.actions
  end

  controller do
    def create
      repo = CourseTeacherRepository.create(permitted_params[:course_teacher])
      @course_teacher = repo.course_teacher
      @course = @course_teacher.course

      if repo.persisted?
        redirect_to course_course_teachers_path(course_id: repo.course_id), notice: "Teacher, #{@course_teacher.teacher.full_name}, is added for Course #{repo.course_id}"
      else
        render action: :new
      end
    end
  end
end
