module Client
  module V1
    class LessonSchedule < Grape::API
      include Helper

      helpers do
        def repository
          LessonScheduleRepository.find(params[:id])
        end

        def lesson_schedule_params
          ActionController::Parameters.new(params)
            .require(:lesson_schedule).permit(:course_id, :started_time, :ended_time)
        end
      end

      resource :lesson_schedules do
        post do
          repository = LessonScheduleRepository.create(lesson_schedule_params.merge(status: :insert))
          respond_with(repository, :persisted?)
        end

        get ':id' do
          respond_with(repository)
        end

        post ':id/cancel' do
          respond_with(repository, :cancel!)
        end
      end
    end
  end
end
