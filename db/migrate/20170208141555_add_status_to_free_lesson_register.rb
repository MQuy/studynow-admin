class AddStatusToFreeLessonRegister < ActiveRecord::Migration
  def change
    add_column :free_lesson_registers, :status, :integer, default: 0
  end
end
