module CarrierWaveDirect
  module Uploader
    attr_reader :filename

    def full_filename(for_file)
      [version_name, for_file].compact.join('_')
    end

    def url(opts = {})
      model.send("remote_#{mounted_as}_tmp_url").presence || super
    end
  end
end
