class Notification < ActiveRecord::Base
  enum action: [:created]

  belongs_to :notificable, polymorphic: true
  belongs_to :user
  belongs_to :creator, class_name: User

  scope :read, -> { where(read: true) }
  scope :unread, -> { where(read: false) }
end
