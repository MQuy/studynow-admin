ActiveAdmin.register Payment do
  permit_params :user_id, :category, :price_units, :price_currency

  filter :user
  filter :category
  filter :state

  action_item :payment, only: :show do
    if payment.pending?
      link_to('Accept', pay_payment_path(payment.id), method: :post, class: 'mg-left-4px')
    end
  end

  sidebar :versionate, partial: "admin/version", only: :show, if: -> { current_admin_user.superadmin? }

  index do
    id_column
    column :user
    column('State') do |p|
      status_tag(p.state, class: admin_statuses[p.state])
    end
    column('Price') { |p| money_format(p.price) }
    column :stated_at
    actions do |p|
      if p.pending?
        link_to 'Accept', pay_payment_path(p.id), method: :post
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :user
      row('Category') { |p| p.category.titleize }
      row('State') do |p|
        status_tag(p.state, class: admin_statuses[p.state])
      end
      row('Price') { |p| money_format(p.price) }
      row :stated_at
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Payment Details' do
      f.input :user, collection: Student.all
      f.input :price_units
      f.input :price_currency, collection: Payment::CURRENIES, include_blank: false
      f.input :category, as: :select, collection: payment_category_opts, include_blank: false, selected: f.object.category || 'deposit', wrapper_html: { class: 'hide' }
    end
    f.actions
  end

  member_action :pay, method: :post do
    if PaymentRepository.load(resource).deposit
      redirect_to payments_path, notice: "Payment is paid successfully."
    else
      redirect_to payments_path, error: "There is an error when paying."
    end
  end

  controller do
    def create
      repo = PaymentRepository.create(permitted_params[:payment])
      @payment = repo.payment

      if repo.persisted?
        redirect_to payments_path, notice: "Payment is created successfully."
      else
        render action: :new
      end
    end
  end
end
