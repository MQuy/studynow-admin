# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170208170604) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role"
    t.string   "full_name"
    t.text     "teacher_ids",            default: [],              array: true
    t.string   "slug"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "attach_files", force: :cascade do |t|
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.integer  "user_id"
    t.string   "path"
    t.string   "content_type"
    t.integer  "file_size"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remote_path_tmp_url"
  end

  add_index "attach_files", ["attachable_id", "attachable_type"], name: "index_attach_files_on_attachable_id_and_attachable_type", using: :btree
  add_index "attach_files", ["user_id"], name: "index_attach_files_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "course_notes", force: :cascade do |t|
    t.integer  "course_id"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "course_notes", ["course_id"], name: "index_course_notes_on_course_id", using: :btree

  create_table "course_schedules", force: :cascade do |t|
    t.integer   "course_id"
    t.int4range "block_range"
    t.datetime  "created_at"
    t.datetime  "updated_at"
  end

  add_index "course_schedules", ["course_id"], name: "index_course_schedules_on_course_id", using: :btree

  create_table "course_teachers", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "teacher_id"
    t.datetime "assigned_at"
    t.datetime "expired_at"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "data",        default: {}
  end

  add_index "course_teachers", ["course_id"], name: "index_course_teachers_on_course_id", using: :btree
  add_index "course_teachers", ["teacher_id"], name: "index_course_teachers_on_teacher_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.integer  "student_id"
    t.string   "code"
    t.boolean  "trial"
    t.integer  "number_of_lessons"
    t.date     "started_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone"
    t.integer  "number_of_reschedule", default: 4
    t.integer  "status",               default: 0
    t.integer  "creator_id"
    t.integer  "price_units",          default: 0,     null: false
    t.string   "price_currency",       default: "VND", null: false
    t.integer  "lesson_duration"
  end

  add_index "courses", ["student_id"], name: "index_courses_on_student_id", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.string   "full_name"
    t.string   "email"
    t.string   "phone_number"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",       default: 0
  end

  create_table "free_lesson_registers", force: :cascade do |t|
    t.string   "full_name"
    t.string   "phone_number"
    t.string   "email"
    t.string   "skype"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "programs",     default: [], array: true
    t.integer  "status",       default: 0
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "homework_students", force: :cascade do |t|
    t.integer  "homework_id"
    t.integer  "student_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "score",       precision: 5, scale: 2, default: 0.0
  end

  create_table "homeworks", force: :cascade do |t|
    t.integer  "admin_user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
  end

  create_table "lesson_schedules", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "teacher_id"
    t.integer  "student_id"
    t.integer  "cause_by_id"
    t.datetime "started_time"
    t.datetime "ended_time"
    t.integer  "status",         default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price_units",    default: 0,     null: false
    t.string   "price_currency", default: "VND", null: false
  end

  add_index "lesson_schedules", ["course_id"], name: "index_lesson_schedules_on_course_id", using: :btree
  add_index "lesson_schedules", ["student_id"], name: "index_lesson_schedules_on_student_id", using: :btree
  add_index "lesson_schedules", ["teacher_id"], name: "index_lesson_schedules_on_teacher_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "notificable_id"
    t.string   "notificable_type"
    t.integer  "user_id"
    t.integer  "creator_id"
    t.integer  "action"
    t.boolean  "read",             default: false
    t.json     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "notifications", ["notificable_id", "notificable_type"], name: "index_notifications_on_notificable_id_and_notificable_type", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category",       default: 0
    t.integer  "state",          default: 0
    t.datetime "stated_at"
    t.integer  "price_units",    default: 0,     null: false
    t.string   "price_currency", default: "VND", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.integer  "author_id"
    t.string   "title"
    t.string   "cover_image"
    t.text     "content"
    t.text     "markdown"
    t.integer  "category"
    t.string   "slug"
    t.string   "brief"
    t.text     "tags",        default: [], array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status"
  end

  add_index "posts", ["author_id"], name: "index_posts_on_author_id", using: :btree

  create_table "schema_versions", id: false, force: :cascade do |t|
    t.string "version", null: false
  end

  add_index "schema_versions", ["version"], name: "unique_schema_migrations", unique: true, using: :btree

  create_table "teacher_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "price_units",    default: 0,     null: false
    t.string   "price_currency", default: "VND", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teacher_profiles", ["user_id"], name: "index_teacher_profiles_on_user_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "payable_id"
    t.string   "payable_type"
    t.integer  "price_units",    default: 0,     null: false
    t.string   "price_currency", default: "VND", null: false
    t.integer  "category",                       null: false
    t.string   "reason"
    t.json     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transactions", ["payable_id", "payable_type"], name: "index_transactions_on_payable_id_and_payable_type", using: :btree

  create_table "user_providers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "provider"
    t.string   "uid"
    t.string   "token"
    t.string   "secret"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_providers", ["provider"], name: "index_user_providers_on_provider", using: :btree
  add_index "user_providers", ["user_id"], name: "index_user_providers_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "full_name"
    t.string   "phone_number"
    t.string   "skype"
    t.string   "address"
    t.string   "avatar"
    t.string   "authentication_token"
    t.string   "type"
    t.string   "slug"
    t.string   "nickname"
    t.integer  "status",                 default: 0
    t.string   "code"
    t.integer  "creator_id"
    t.string   "referral_code"
    t.string   "currency"
    t.string   "time_zone"
    t.string   "remote_avatar_tmp_url"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "wallets", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "price_units",    default: 0,     null: false
    t.string   "price_currency", default: "VND", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wallets", ["user_id"], name: "index_wallets_on_user_id", using: :btree

  create_table "weekly_schedules", force: :cascade do |t|
    t.integer   "teacher_id"
    t.int4range "block_range"
    t.datetime  "created_at"
    t.datetime  "updated_at"
  end

  add_index "weekly_schedules", ["teacher_id"], name: "index_weekly_schedules_on_teacher_id", using: :btree

end
