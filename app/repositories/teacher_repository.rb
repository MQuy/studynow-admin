class TeacherRepository
  extend Helpers::RepoAttr

  attr_record :teacher

  def initialize(teacher_params = {})
    @teacher = Teacher.new(teacher_params)
  end

  def update_weekly_schedules(params)
    teacher.update(weekly_schedules_attributes: params)
  end

  def find_course(student_slug)
    teacher.courses.in_process.joins(:student)
      .find_by(users: { slug: student_slug })
  end
end
