class RenameReferralCodeToCodeInAdminuser < ActiveRecord::Migration
  def change
    rename_column :users, :referral_code, :code
  end
end
