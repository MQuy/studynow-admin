class Payment < ActiveRecord::Base

  CURRENIES = ['VND', 'PHP', 'USD']

  monetize :price_units

  enum state: [:pending, :approved, :rejected, :expired]
  enum category: [:deposit, :refund]

  belongs_to :user

  before_save :update_stated_at

  has_paper_trail on: [:create, :update, :destroy]

  private

  def update_stated_at
    self.stated_at = Time.current if state_changed?
  end
end
