class AddStatusToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :status, :integer, default: 0
  end
end
