class TeacherSerializer < ActiveModel::Serializer
  has_many :weekly_schedules
  has_many :lesson_schedules do
    object.lesson_schedules.be_learned.includes(:teacher, :student, :course)
  end

  attributes :id, :email, :full_name, :nickname, :authentication_token, :skype, :role,
    :slug, :address, :avatar_url, :sign_in_count, :phone_number, :timezone

  def avatar_url
    object.avatar.url
  end

  def role
    object.type.underscore
  end

  def timezone
    ActiveSupport::TimeZone::MAPPING[object.time_zone]
  end
end
