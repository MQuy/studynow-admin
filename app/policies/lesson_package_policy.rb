class LessonPackagePolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def destroy?
    user.superadmin?
  end
end
