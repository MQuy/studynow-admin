class AddCodeToHomework < ActiveRecord::Migration
  def change
    add_column :homeworks, :code, :string
  end
end
