# encoding: utf-8

class FileUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader

  storage :fog

  def store_dir
    model_path = model.id.presence || 'temp'
    "uploads/#{model.class.to_s.underscore}/#{model_path}/#{mounted_as}"
  end
end
