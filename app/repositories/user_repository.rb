class UserRepository
  extend Helpers::RepoAttr

  attr_record :user

  class << self
    def create_without_password(email, full_name, time_zone = nil, type = 'Student')
      password = Devise.friendly_token.first(8)

      create(email: email, password: password, full_name: full_name, time_zone: time_zone, type: type)
    end

    def create(params = {})
      repo = new(params)
      user = repo.user
      ActiveRecord::Base.transaction do
        if user.save!
          user.create_wallet(price_units: 0, price_currency: user.currency)
          user.create_teacher_profile(price_units: 0, price_currency: user.currency) if user.teacher?
        end
      end
      MailerQueue::UserWorker.delay.welcome(user.id)
    ensure
      return repo
    end
  end

  def initialize(user_params = {})
    @user = User.new(user_params)
  end

  def upcoming_schedules(number = 2)
    user
      .lesson_schedules.be_learned
      .includes(:course, :teacher, :student)
      .available.order(:started_time).limit(number)
  end
end
