class AgencyPolicy < ApplicationPolicy
  include Helpers::SuperAdminPolicy

  class Scope < Scope

    def resolve
      scope.agencies
    end
  end
end
