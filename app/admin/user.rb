ActiveAdmin.register User do
  menu priority: 10000
  permit_params :email, :password, :password_confirmation, :full_name, :type, :skype, :phone_number,
    :nickname, :status, :creator_id, :referral_code, :currency, :time_zone, :address,
    teacher_profile_attributes: [:price_units, :price_currency]

  filter :email
  filter :full_name
  filter :phone_number
  filter :skype
  filter :address
  filter :nickname
  filter :code
  filter :currency
  filter :time_zone

  scope :teachers
  scope('Students', default: true) { |scope| scope.students.where.not(status: 0) }
  scope('Pending') { |scope| scope.students.pending }

  action_item :weekly_schedules, only: :show do
    user.teacher? ? link_to('Weekly Schedules', user_weekly_schedules_path(user_id: user.id), class: 'mg-left-4px') : ''
  end

  action_item :active, only: :show do
    user.pending? ? link_to('Active', active_user_path(user.id), method: :post, class: 'mg-left-4px') : ''
  end

  action_item :transactions, only: :show do
    link_to('Transactions', user_transactions_path(user_id: user.id), class: 'mg-left-4px')
  end

  sidebar :versionate, partial: "admin/version", only: :show, if: -> { current_admin_user.superadmin? }

  index(row_class: ->(record) { 'red' if record.student? && StudentRepository.load(record).out_of_in_process_courses? }) do
    id_column
    column :full_name
    if params[:scope] == 'teachers'
      render 'admin/users/teachers', context: self
    elsif params[:scope] == 'pending'
      render 'admin/users/pending', context: self
    else
      render 'admin/users/students', context: self
    end
    column('Type') do |user|
      status_tag(user.type, class: admin_statuses[user.type.downcase])
    end
    column :created_at
    actions do |user|
      user.pending? ? link_to('Active', active_user_path(user.id), method: :post) : ''
    end
  end

  show do
    attributes_table do
      row :id
      row :email
      row :full_name
      row :nickname
      row :currency
      row :time_zone
      row :phone_number
      row :skype
      row :address
      row('Type') do |user|
        status_tag(user.type, class: admin_statuses[user.type.downcase])
      end
      row :status
      if user.student?
        row :code
        row :referrer
      end
    end
    panel 'Wallet' do
      attributes_table_for resource do
        row('Money') { |user| money_format(user.wallet.price) }
      end
    end
    if resource.teacher?
      panel 'Profile' do
        attributes_table_for resource do
          row('Price per hour') { |teacher| money_format(teacher.teacher_profile.price) }
        end
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'User Details' do
      f.input :email, input_html: { readonly: f.object.persisted? }
      f.input :full_name
      f.input :nickname
      f.input :skype
      f.input :phone_number
      f.input :address
      f.input :time_zone, as: :select, collection: ActiveSupport::TimeZone.all.map { |tz| [tz.to_s, tz.name] }, selected: 'Hanoi', include_blank: false
      f.input :currency, collection: Payment::CURRENIES, include_blank: false
      unless f.object.persisted?
        f.input :password
        f.input :password_confirmation
        f.input :type, as: :select, collection: user_type_opts, include_blank: false, selected: f.object.type || 'Student'
        f.input :status, as: :hidden, input_html: { value: :active }
      end
      f.input :referral_code
    end
    if f.object.persisted? && f.object.teacher?
      render 'admin/users/teacher', context: self, f: f
    end
    f.actions
  end

  member_action :active, method: :post do
    if resource.active!
      redirect_to users_path, notice: "User, #{resource.full_name}, is actived successfully."
    else
      redirect_to users_path, error: 'Cannot active user.'
    end
  end

  controller do
    def create
      params[:user][:creator_id] = current_admin_user.id
      repo = UserRepository.create(permitted_params[:user])
      @user = repo.user

      if repo.persisted?
        redirect_to user_path(repo.slug), notice: "User, #{repo.full_name}, is created successfully."
      else
        render action: :new
      end
    end

    def destroy
      if resource.destroy
        prev_args = Rack::Utils.parse_query URI.parse(request.referrer).query
        redirect_to users_path(prev_args), notice: "User, #{resource.full_name}, is deleted successfully."
      else
        redirect_to user_path(resource), notice: "There is an error when deleting #{resource.full_name}."
      end
    end
  end
end
