class AddTeacherIdsToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :teacher_ids, :text, array: true, default: []
  end
end
