class AttachFileRepository
  extend Helpers::RepoAttr

  attr_record :attach_file

  def initialize(attach_file_params = {})
    @attach_file = AttachFile.new(attach_file_params)
  end

  def update(params)
    attach_file.update(params)
  end
end

