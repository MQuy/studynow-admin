class CourseSerializer < ActiveModel::Serializer
  has_many :course_notes
  has_many :lesson_schedules

  attributes :id, :remain_of_lessons, :number_of_lessons, :started_date, :code
  attributes :active_teacher, :student, :price, :number_of_hours

  def active_teacher
    TeacherSerializer.new(object.active_teacher)
  end

  def student
    UserArraySerializer.new(object.student)
  end

  def price
    object.price.format
  end

  def number_of_hours
    object.lesson_schedules.be_learned.all.map(&:length).inject(:+).to_f / 60
  end
end
