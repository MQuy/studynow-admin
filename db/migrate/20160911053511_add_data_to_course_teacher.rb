class AddDataToCourseTeacher < ActiveRecord::Migration
  def change
    add_column :course_teachers, :data, :json, default: {}
  end
end
