module Client
  module V1
    class User < Grape::API
      include Helper

      helpers do
        def create_params
          ActionController::Parameters.new(params)
            .require(:user).permit(:email, :nickname, :full_name, :password, :password_confirmation, :type)
        end

        def user_params
          ActionController::Parameters.new(params).require(:user).permit!
        end

        def profile_serializer(signed_user = current_user)
          render signed_user, serializer: "#{signed_user.type}Serializer".constantize
        end

        def repository
          UserRepository.find(params[:id])
        end
      end

      resource :users do
        put ':id' do
          current_user.update_attributes(user_params)
          if current_user.valid?
            profile_serializer
          else
            { errors: current_user.errors.to_a }
          end
        end

        post :sign_in do
          user = ::User.students_or_teachers.find_by(email: user_params[:email]&.downcase)
          if user && user.valid_password?(user_params[:password])
            profile_serializer(user)
          else
            render({ errors: ['Email or Passsword is not correct'] }, serializer: EmptySerializer)
          end
        end

        post :sign_up do
          repository = UserRepository.create(create_params)
          if repository.valid?
            profile_serializer(repository.user)
          else
            render({ errors: repository.errors }, serializer: EmptySerializer)
          end
        end

        get :me do
          if current_user
            profile_serializer
          else
            error!('401 Unauthorized', 401)
          end
        end

        post :forgot_password do
          repository = UserRepository.find_by(email: params[:email])
          if repository
            UserMailer.delay.reset_password(repository.id)
            render({ messages: ['We sent you a link to reset your password'] }, serializer: EmptySerializer)
          else
            render({ errors: ['We can not this email in our system'] }, serializer: EmptySerializer)
          end
        end

        post :reset_password do
          repository = UserRepository.find_by(authentication_token: user_params[:token])
          if repository
            if repository.update(password: user_params[:password])
              profile_serializer(repository.user)
            else
              render({ errors: repository.errors }, serializer: EmptySerializer)
            end
          else
            render({ errors: ['This token is not correct'] }, serializer: EmptySerializer)
          end
        end

        get ':id/courses' do
          courses = Search::CourseService.new(repository.user, params).execute
          preload = courses.includes(:student, :course_notes, lesson_schedules: [:student, :teacher])
          render preload, each_serializer: CourseArraySerializer
        end

        get ':id/lesson_schedules' do
          lesson_schedules = SchedulesInRangeService.new(repository.user, params).execute

          render lesson_schedules, each_serializer: LessonScheduleSerializer
        end

        get ':id/upcoming_schedules' do
          upcoming_schedules = UpcomingSchedulesFilterService.new(repository.user, params).execute

          render upcoming_schedules, each_serializer: LessonScheduleSerializer
        end

        post ':id/mark_as_read' do
          repository.user.notifications.update(read: true)
        end
      end
    end
  end
end
