ActiveAdmin.register Agency do
  menu priority: 10001
  permit_params :full_name, :email, :password, :password_confirmation, :status, :type

  filter :full_name
  filter :email
  filter :code
  filter :phone_number

  action_item :transactions, only: :show do
    link_to('Transactions', user_transactions_path(user_id: agency.id), class: 'mg-left-4px')
  end

  index do
    id_column
    column :full_name
    column :email
    column :code
    column :phone_number
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :code
      row :full_name
      row :nickname
      row :phone_number
      row :skype
      row :address
      row :status
    end
    panel 'Wallet' do
      attributes_table_for agency do
        row('Money') { |agency| money_format(agency.wallet.price) }
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'User Details' do
      f.input :full_name
      f.input :email
      unless f.object.persisted?
        f.input :password
        f.input :password_confirmation
      end
      f.input :status, as: :hidden, input_html: { value: :active }
      f.input :type, as: :hidden, input_html: { value: 'Agency' }
    end
    f.actions
  end

  controller do
    def create
      params[:agency][:creator_id] = current_admin_user.id
      repo = AgencyRepository.create(permitted_params[:agency])
      @agency = repo.agency

      if repo.persisted?
        redirect_to agency_path(repo.slug), notice: "Agency is created successfully."
      else
        render action: :new
      end
    end
  end
end
