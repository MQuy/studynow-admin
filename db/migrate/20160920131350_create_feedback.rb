class CreateFeedback < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :full_name
      t.string :email
      t.string :phone_number
      t.text :content
      t.timestamps
    end
  end
end
