class CreateCourseTeacher < ActiveRecord::Migration
  def change
    create_table :course_teachers do |t|
      t.belongs_to :course
      t.belongs_to :teacher
      t.datetime :assigned_at
      t.datetime :expired_at
      t.integer :status

      t.timestamps
    end
  end
end
