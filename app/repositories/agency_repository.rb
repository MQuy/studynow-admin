class AgencyRepository
  extend Helpers::RepoAttr

  attr_record :agency

  class << self
    def create(params = {})
      repo = new(params)
      agency = repo.agency
      ActiveRecord::Base.transaction do
        if agency.save!
          agency.create_wallet(price_units: 0, price_currency: agency.currency)
        end
      end
    ensure
      return repo
    end
  end

  def initialize(agency_params = {})
    @agency = Agency.new(agency_params)
  end

  def referral_ratio(student_id, course_id = nil)
    course_ids = agency.transactions.course_referrer.with_student(student_id).map { |t| t.data['course_id'] }
    (course_ids + [course_id]).compact.uniq.length > 1 ? 0.08 : 0.1
  end
end

