class CourseNoteRepository
  extend Helpers::RepoAttr

  attr_record :course_note

  def initialize(course_note_params = {})
    @course_note = CourseNote.new(course_note_params)
  end
end
