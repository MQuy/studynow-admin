class CreateWeeklySchedule < ActiveRecord::Migration
  def change
    create_table :weekly_schedules do |t|
      t.belongs_to :teacher
      t.int4range :block_range

      t.timestamps
    end
  end
end
