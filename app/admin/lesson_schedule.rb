ActiveAdmin.register LessonSchedule do
  belongs_to :course
  permit_params :teacher_id, :student_id, :started_date, :started_hour, :status, :course_id, :length
  actions :all, except: [:edit]

  index do
    id_column
    column :teacher
    column :student
    column :started_time do |ls|
      ls.started_time.in_time_zone(ls.course.time_zone).strftime("%B %d, %Y %H:%M %z")
    end
    column :ended_time do |ls|
      ls.ended_time.in_time_zone(ls.course.time_zone).strftime("%B %d, %Y %H:%M %z")
    end
    column :status do |ts|
      status_tag(ts.status, class: admin_statuses[ts.status])
    end
    actions do |lesson_schedule|
      course = lesson_schedule.course
      (lesson_schedule.cancel? ? '' : link_to('Cancel', cancel_course_lesson_schedule_path(lesson_schedule.id, course_id: course.id), method: :post, class: 'mg-left-2px'))
    end
  end

  show do
    attributes_table do
      row :id
      row :teacher
      row :student
      row :started_time do |ls|
        ls.started_time.in_time_zone(ls.course.time_zone).strftime("%B %d, %Y %H:%M")
      end
      row :ended_time do |ls|
        ls.ended_time.in_time_zone(ls.course.time_zone).strftime("%B %d, %Y %H:%M")
      end
      row :status do |ts|
        status_tag(ts.status, class: admin_statuses[ts.status])
      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Teaching Schedule' do
      f.input :time_zone, input_html: { value: f.object.course.time_zone, disabled: true }
      f.input :started_date, as: :datepicker
      f.input :started_hour, as: :string, input_html: { class: 'time-picker' }
      f.input :length, label: 'Duration', input_html: { value: f.object.course.lesson_duration }
      f.input :status, as: :hidden, input_html: { value: 'insert' }
      f.input :course_id, as: :hidden, input_html: { value: params[:course_id] }
    end
    f.actions
  end

  member_action :cancel, method: :post do
    repo = LessonScheduleRepository.load(resource)
    if repo.cancel!
      redirect_to :back, notice: "Schedule is cancelled successfully."
    else
      redirect_to :back, alert: repo.errors
    end
  end

  controller do
    def create
      repo = LessonScheduleRepository.create(permitted_params[:lesson_schedule])
      @lesson_schedule = repo.lesson_schedule
      @course = @lesson_schedule.course

      if repo.persisted?
        redirect_to course_lesson_schedules_path(repo.course_id), notice: 'Lesson schedule is created successfully.'
      else
        render action: :new
      end
    end
  end
end
