class StudentSerializer < ActiveModel::Serializer
  attributes :id, :email, :full_name, :nickname, :authentication_token, :skype, :role, :slug,
    :address, :avatar_url, :sign_in_count, :phone_number, :timezone
  attributes :price_units

  def avatar_url
    object.avatar.url
  end

  def role
    object.type.underscore
  end

  def price_units
    object.wallet.price_units
  end

  def timezone
    ActiveSupport::TimeZone::MAPPING[object.time_zone]
  end
end
