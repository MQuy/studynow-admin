module UniqueCode
  extend ActiveSupport::Concern

  included do
    before_create :generate_unique_code
  end

  def generate_unique_code
    letters = (0..9).to_a + ('A'..'Z').to_a
    self.code = loop do
      token = letters.sample(8).join
      break token unless self.class.exists?(code: token)
    end
  end
end
