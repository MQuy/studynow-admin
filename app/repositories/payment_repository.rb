class PaymentRepository
  extend Helpers::RepoAttr

  attr_record :payment

  def initialize(payment_params = {})
    @payment = Payment.new(payment_params)
  end

  def deposit
    ActiveRecord::Base.transaction do
      if payment.approved!
        WalletRepository
          .load(payment.user.wallet)
          .receive(payment.price, :payment_deposit)
      end
    end
    payment.valid?
  end
end
