class Transaction < ActiveRecord::Base
  enum category: [:payment_deposit, :course_deposit, :course_referrer, :course_refund, :withdraw, :course_delete]

  monetize :price_units

  belongs_to :payable, polymorphic: true

  scope :with_student, -> (student_id) { where("data ->> 'student_id' = ?", student_id.to_s) }
  scope :with_students, -> (student_ids) { where("data ->> 'student_id' IN ('#{student_ids.join("', '")}')") }

  validates :category, presence: true
end
