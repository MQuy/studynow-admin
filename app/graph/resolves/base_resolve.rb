class BaseResolve
  attr_reader :obj, :args, :context

  class << self
    def call(obj, args, context)
      new(obj, args, context).execute
    end
  end

  def initialize(obj, args, context)
    @obj = obj
    @args = args
    @context = context
  end
end
