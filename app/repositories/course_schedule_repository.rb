class CourseScheduleRepository
  extend Helpers::RepoAttr

  attr_record :course_schedule
  attr_accessor :time_zone

  class << self
    def load(course_schedule, lesson_duration, time_zone = Time.zone.name)
      repo = new({}, lesson_duration, time_zone)
      repo.course_schedule = course_schedule
      repo.course_schedule.block_range_length = lesson_duration
      repo.convert_into_zero_zone
      repo
    end
  end

  def initialize(course_schedule_params = {}, lesson_duration, time_zone)
    @course_schedule = CourseSchedule.new(course_schedule_params)
    @time_zone = time_zone

    course_schedule.block_range_length = lesson_duration
    convert_into_zero_zone
  end

  def convert_into_zero_zone
    distance = Time.now.in_time_zone(time_zone).utc_offset / 3600 * 2
    course_schedule.shift_on_distance(distance)
  end
end
