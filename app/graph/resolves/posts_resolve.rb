class PostsResolve < BaseResolve
  attr_accessor :posts

  def execute
    filter_category if args[:category].present?
    filter_tag if args[:tag].present?
    filter_author if args[:author].present?
    filter_page if args[:page].present?
    posts
  end

  private

  def filter_category
    @posts = posts.where(category: Post.categories[args[:category].downcase])
  end

  def filter_tag
    @posts = posts.where("tags @> '{#{args[:tag].downcase}}'")
  end

  def filter_author
    @posts = posts.where("admin_users.slug ilike ?", args[:author])
  end

  def filter_page
    @posts = posts.offset(args[:page].to_i * 10)
  end

  def posts
    @posts ||= Post.joins(:author)
                 .includes(:author)
                 .order(created_at: :desc)
                 .limit(10)
  end
end
