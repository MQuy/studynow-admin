ActiveAdmin.register CourseNote do
  belongs_to :course
  permit_params :id, :course_id, :title, :description

  index do
    id_column
    column :title
    column :description
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :description
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    inputs 'Course Note' do
      f.input :course_id, input_html: { hidden: true }
      f.input :title
      f.input :description
    end
    f.actions
  end

  controller do
    def create
      repo = CourseNoteRepository.create(permitted_params[:course_note])
      @course_note = repo.course_note

      if repo.persisted?
        redirect_to course_course_notes_path(repo.course_id), notice: "Course note is created successfully."
      else
        render action: :new
      end
    end
  end
end
