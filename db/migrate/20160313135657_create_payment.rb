class CreatePayment < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.belongs_to :user
      t.integer :category, default: 0
      t.integer :number_of_lessons
      t.integer :state, default: 0
      t.datetime :stated_at
      t.monetize :price

      t.timestamps
    end
  end
end
