class Post < ActiveRecord::Base
  extend FriendlyId

  enum category: [:general]
  enum status: [:draft, :publish]

  belongs_to :author, class_name: AdminUser

  mount_uploader :cover_image, LocalUploader
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]
  has_paper_trail on: [:create, :update, :destroy]

  def slug_candidates
    [
      :title,
      [:id, :title]
    ]
  end

  def should_generate_new_friendly_id?
    title_changed? || super
  end

  def normalize_friendly_id(string)
    string.to_url
  end
end
