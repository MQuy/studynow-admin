ActiveAdmin.register WeeklySchedule do
  belongs_to :user
  permit_params :teacher_id, :block_range_day_begin, :block_range_day_end, :block_range_time_begin, :block_range_time_end

  index do
    id_column
    column :teacher
    column('Available Time') { |ws| block_range_format(ws, ws.teacher.time_zone) }
    actions
  end

  show do
    attributes_table do
      row :id
      row :teacher
      row('Available Time') { |ws| block_range_format(ws, ws.teacher.time_zone) }
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Available Time' do
      f.input :teacher_id, as: :hidden, input_html: { value: f.object.teacher.id }
      f.input :time_zone, input_html: { value: f.object.teacher.time_zone, disabled: true }
      div class: 'group-margin'
      f.input :block_range_day_begin, label: 'Block range begin', as: :select, collection: calendar_days_opts, wrapper_html: { class: 'd-inline' }, input_html: { class: 'width-200px' }, label_html: { class: 'block-range-label' }
      f.input :block_range_time_begin, label: false, :as => :string, wrapper_html: { class: 'd-inline' }, :input_html => {:class => 'time-picker width-200px'}
      div class: 'group-margin'
      f.input :block_range_day_end, label: 'Block range end', :as => :select, collection: calendar_days_opts, wrapper_html: { class: 'd-inline' }, :input_html => {:class => 'width-200px'}, label_html: { class: 'block-range-label' }
      f.input :block_range_time_end, label: false, :as => :string, wrapper_html: { class: 'd-inline' }, :input_html => {:class => 'time-picker width-200px'}
      div class: 'group-margin'
    end
    f.actions
  end

  controller do
    def create
      repo = WeeklyScheduleRepository.new(permitted_params[:weekly_schedule])

      if repo.save
        @weekly_schedule = repo.weekly_schedule
        redirect_to user_weekly_schedules_path(repo.teacher_id), notice: "Weekly schedule is created successfully."
      else
        render action: :new
      end
    end

    def edit
      distance = Time.now.in_time_zone(resource.teacher.time_zone).utc_offset / 3600 * 2
      resource.shift_on_distance(-distance)
      super
    end

    def update
      distance = Time.now.in_time_zone(resource.teacher.time_zone).utc_offset / 3600 * 2
      resource.assign_attributes(permitted_params[:weekly_schedule])
      resource.shift_on_distance(distance)

      if resource.save
        redirect_to user_weekly_schedule_path(resource.id, user_id: resource.teacher_id), notice: "Weekly schedule is updated successfully."
      else
        render action: :new
      end
    end
  end
end
