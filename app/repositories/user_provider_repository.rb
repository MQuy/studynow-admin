class UserProviderRepository
  extend Helpers::RepoAttr

  attr_record :user_provider

  class << self
    def create(args)
      time_zone = ActiveSupport::TimeZone[args[:time_zone] || 0].name
      repository = UserRepository.find_by(email: args[:email]) || UserRepository.create_without_password(args[:email], args[:name], time_zone)
      user_provider = repository.user.user_providers.find_or_create_by(provider: 'facebook').tap do |up|
        up.update_attributes(token: args[:token], uid: args[:uid])
      end
      load(user_provider)
    end
  end

  def initialize(user_provider_params = {})
    @user_provider = UserProvider.new(user_provider_params)
  end
end
