class AddTmpUrlToUser < ActiveRecord::Migration
  def change
    add_column :users, :remote_avatar_tmp_url, :string
  end
end
