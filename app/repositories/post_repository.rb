class PostRepository
  extend Helpers::RepoAttr

  attr_record :post

  class << self
    def create(params = {})
      repo = new(params)
      post = repo.post
      ActiveRecord::Base.transaction do
        if repo.save!
          post_url = PathHelper.post_url(post)
          PrerenderWorker.perform_async(post_url)
        end
      end
    ensure
      return repo
    end
  end

  def initialize(post_params = {})
    @post = Post.new(post_params)
  end
end
