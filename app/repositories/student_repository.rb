class StudentRepository
  extend Helpers::RepoAttr

  REMAIN_LESSONS = 2

  attr_record :student

  def initialize(student_params = {})
    @student = Student.new(student_params)
  end

  def out_of_in_process_courses?
    !student.courses.any? { |c| c.remain_of_lessons >= REMAIN_LESSONS }
  end

  def find_course(teacher_slug)
    student.courses.in_process.joins(:teacher)
      .find_by(users: { slug: teacher_slug })
  end

  def deposit_course(course)
    WalletRepository
      .load(student.wallet)
      .withdraw(course.price, :course_deposit)
  end

  def referral_ratio
    0.05
  end
end
