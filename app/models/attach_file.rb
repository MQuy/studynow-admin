class AttachFile < ActiveRecord::Base
  include CarrierWaveDirectConcern

  belongs_to :user

  before_save :update_file_info

  mount_uploader :path, FileUploader

  def file_name
    (attributes['remote_path_tmp_url'] || attributes['path'] || '').split('/').last
  end

  private

  def update_file_info
    if path.present? && path_changed?
      self.content_type = path.file.content_type
      self.file_size = path.file.size
    end
  end
end

