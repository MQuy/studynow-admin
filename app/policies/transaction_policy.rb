class TransactionPolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def create?
    false
  end

  def destroy?
    false
  end

  def update?
    false
  end
end
