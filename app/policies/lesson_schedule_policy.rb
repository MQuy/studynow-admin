class LessonSchedulePolicy < ApplicationPolicy
  include Helpers::AdminPolicy

  def index?
    user.admin? || user.manager?
  end

  def show?
    user.admin? || user.manager?
  end

  def cancel?
    user.admin?
  end

  def destroy?
    user.superadmin?
  end
end

