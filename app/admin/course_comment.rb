ActiveAdmin.register Comment, as: 'CourseComment' do
  belongs_to :course
  actions :all, except: [:edit, :destroy]

  filter :content
  filter :created_at

  index do
    selectable_column
    id_column
    column :content
    column('Attachment') do |comment|
      if comment.attach_file
        link_to 'Download', comment.attach_file.path.url, download: true
      end
    end
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :user
      row :content
      row('Attachment') do |comment|
        if comment.attach_file
          link_to 'Download', comment.attach_file.path.url, download: true
        end
      end
      row :created_at
    end
    active_admin_comments
  end
end
