module ActiveAdmin
  class PagePolicy < ApplicationPolicy

    def idnex?
      true
    end

    def show?
      true
    end

    def teacher?
      true
    end

    def student?
      true
    end
  end
end
