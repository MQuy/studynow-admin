class WeeklyScheduleSerializer < ActiveModel::Serializer
  attributes :id, :block_range_begin, :block_range_end, :teacher_id
end
