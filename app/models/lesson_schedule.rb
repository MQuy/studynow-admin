class LessonSchedule < ActiveRecord::Base
  enum status: [:official, :insert, :cancel]

  monetize :price_units
  has_paper_trail on: [:create, :update]

  belongs_to :course, class_name: Course
  belongs_to :teacher, class_name: Teacher
  belongs_to :student, class_name: Student
  belongs_to :cause_by, class_name: User

  scope :available, -> { where('started_time >= ?', Time.current) }
  scope :be_learned, -> { where(status: [0, 1]) }
  scope :inserted, -> { where(status: 1) }
  scope :cancelled, -> { where(status: 2) }

  before_create :ensure_price_available, if: -> { course.persisted? }

  validates :course_id, :teacher_id, :student_id, presence: true
  validate :have_to_insert_same_range
  validate :available_for_insert, if: -> { insert? }
  validate :available_for_cancel, if: -> { cancel? }

  def length
    ((ended_time - started_time) / 30.minutes).round * 30 rescue 0
  end

  def started_date
    started_time.to_date rescue nil
  end

  def started_hour
    started_time.strftime("%H:%M") rescue nil
  end

  private

  def available_for_cancel
    cancel_lesson_count = course.lesson_schedules.cancelled.count
    if cancel_lesson_count >= course.number_of_reschedule
      errors.add(:base, 'You cannot reschedule more.')
    end
  end

  def available_for_insert
    inserted_lesson_count = course.lesson_schedules.inserted.count
    official_lesson_count = course.lesson_schedules.official.count
    if inserted_lesson_count + official_lesson_count >= course.number_of_lessons
      errors.add(:base, 'You have to cancel firstly.')
    end
  end

  def have_to_insert_same_range
    return unless insert?

    if length == 0
      errors.add(:base, 'Your lesson\'s length is not correct')
    end
  end

  def ensure_price_available
    return unless price_units.zero? && insert?

    self.price = course.price / course.number_of_lessons
  end
end
