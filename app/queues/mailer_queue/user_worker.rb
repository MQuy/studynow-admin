module MailerQueue
  module UserWorker
    extend self
    extend Helper

    def welcome(id)
      user = User.find(id)
      payload = { full_name: user.full_name }
      worker(user.email, payload)
    end
  end
end
